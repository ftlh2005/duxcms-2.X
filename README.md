#DuxCms 2.x

DuxCms 2.x是一款基于Canphp框架与CanApp模式开发的一款中小型内容管理系统

采用CanApp作为底层框架，经过多方面的优化改进，将各个功能模块APP化，通过各个APP之间的API来进行交互，达到的可分离开发扩展极其方便的效果。

集成后台UI组件，集成菜单与权限API，免去设计与开发烦恼

基于php5.2+、mysql5.1+ 开发的,低于此版本无法使用。

#说明

默认后台地址为：http://域名/admin.php

----

#通过SVN也可以同步到最新版

http://code.taobao.org/p/duxcmsv2/src/

----

#交流

文档中心：http://doc.duxcms.com/

BUG反馈：http://bbs.duxcms.com/forum.php?mod=forumdisplay&fid=48

QQ群：131331864
 	
网址：http://www.duxcms.com