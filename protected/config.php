<?php 
$config=array();
//基本配置参数
$config['APP']['HTML_CACHE_PATH']= ROOT_PATH . 'cache/html_cache/';
//基本数据库参数
$config['DB']['DB_CACHE_PATH']= ROOT_PATH . 'cache/db_cache/';
$config['SESSION']['DB_CACHE_PATH']= ROOT_PATH . 'cache/session_cache/';
//基本模板参数
$config['TPL']['TPL_TEMPLATE_SUFFIX']= '.html';
$config['TPL']['TPL_CACHE_PATH']= ROOT_PATH . 'cache/tpl_cache/';

$config_base_files=glob(ROOT_PATH.'inc/base/*.ini.php');
$config_site_files=glob(ROOT_PATH.'inc/config/*.ini.php');
$config_files=array_merge((array)$config_base_files,(array)$config_site_files);
if(!empty($config_files)){
  foreach ($config_files as $value) {
    require ($value);
  }
}
return $config;
