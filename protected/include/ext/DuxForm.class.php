<?php
/**
 * DuxForm.php
 * 表单处理类
 * @author Life <349865361@qq.com>
 * @version 20140116
 */
class DuxForm
{
	static private $elementArray;
	/**
     * 外部html布局
     * @param string $html 布局内html
     */
	public static function layout($html='',$title='',$tip='',$append='')
	{
		if(!empty($html)){
			$html='
			<div class="formitm">
				<label class="lab">'.$title.'：</label>
				<div class="ipt">'.$html.$append.'<p>'.$tip.'</p>
				</div>
			</div>';
		}
		return $html;
	}
	/**
     * 生成含布局html
     * @param string $html 布局内html
     */
	public static function layoutHtml()
	{
		$elementArray=self::$elementArray;
		if(empty($elementArray)){
			return '';
		}
		$html=implode('', $elementArray);
		return $html;
	}
	/**
     * 表单布局
     * @param string $id 表单ID
     * @param string $url 表单URL
     * @param string $html 布局内html
     * @return string html字符串
     */
	public static function layoutForm($id='form',$url)
	{
		$elementArray=self::$elementArray;
		if(empty($elementArray)){
			return '';
		}
		$html=implode('', $elementArray);
		$html='
		<form name="'.$id.'" action="'.$url.'" method="post" id="'.$id.'">
		   	<fieldset>
		   		'.$html.'
		   		<div class="formitm formitm-1">
			        <button class="u-btn" type="submit">立即提交</button>
			        <button class="u-btn s-btn-c3" type="button">重置</button>
					<span class="status" id="status"></span>
		        </div>
			    <input name="relation_key" id="relation_key" type="hidden" value="'.form_time().'" />
		    </fieldset>
		</form>';
		return $html;
	}
	/**
     * 文本框
     * @param string $data['title'] 标题
     * @param string $data['tip'] 默认提示信息
     * @param string $data['name'] name属性
     * @param string $data['value'] 默认值
     * @param string $data['size'] 文本框大小1~6
     * @param string $data['css'] css样式
     * @param string $data['maxlength'] 最大字符数
     * @param string $data['datatype'] 验证规则
     * @param string $data['errormsg'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function text($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['size'])){
			$size='u-ipt-'.$data['size'];
		}
		if(!empty($data['value'])){
			$value=$data['value'];
		}
		if(!empty($data['maxlength'])){
			$maxlength='maxlength="'.$data['maxlength'].'" ';
		}
		if(!empty($data['maxlength'])){
			$maxlength='maxlength="'.$data['maxlength'].'" ';
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}
		$html='
		<input name="'.$name.'" type="text" class="u-ipt '.$size.'" id="'.$name.'" value="'.$value.'" '.$maxlength.$datatype.$errormsg.' />
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 多行文本框
     * @param string $data['title'] 标题
     * @param string $data['tip'] 默认提示信息
     * @param string $data['name'] name属性
     * @param string $data['value'] 默认值
     * @param string $data['size'] 文本框大小1~6
     * @param string $data['datatype'] 验证规则
     * @param string $data['errormsg'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function textarea($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['size'])){
			$size='u-tta-'.$data['size'];
		}
		if(!empty($data['value'])){
			$value=$data['value'];
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}
		$html='<textarea name="'.$name.'" class="u-tta '.$size.'" id="'.$name.'" '.$datatype.$errormsg.'" >'.$value.'</textarea>';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 日期时间
     * @param string $data['title'] 标题
     * @param string $data['tip'] 默认提示信息
     * @param string $data['name'] name属性
     * @param string $data['value'] 默认值
     * @param string $data['size'] 文本框大小1~6
     * @param string $data['css'] css样式
     * @param string $data['maxlength'] 最大字符数
     * @param string $data['datatype'] 验证规则
     * @param string $data['errormsg'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function textTime($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['size'])){
			$size='u-ipt-'.$data['size'];
		}
		if(!empty($data['value'])){
			$value=date('Y-m-d H:i:s',$data['value']);
		}
		if(!empty($data['maxlength'])){
			$maxlength='maxlength="'.$data['maxlength'].'" ';
		}
		if(!empty($data['maxlength'])){
			$maxlength='maxlength="'.$data['maxlength'].'" ';
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}
		$html='
		<input name="'.$name.'" type="text" class="u-ipt u-time '.$size.'" id="'.$name.'" value="'.$value.'" '.$maxlength.$datatype.$errormsg.' />
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 单选
     * @param string $data['name'] name属性
     * @param string $data['list'] 二位数组array(array('title'=>'','value'=>''));
     * @param string $data['value'] 选中值
     * @param string $data['datatype'] 验证规则
     * @param string $data['error'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function radio($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['list'])){
			$list=$data['list'];
		}else{
			return;
		}
		if(!empty($data['value'])){
			$value=$data['value'];
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}

		$html='';
		if(!empty($list)||is_array($list)){
			foreach ($list as $val) {
				$html.='<label class="u-opt" '.$datatype.$errormsg.' >
				<input type="radio" name="'.$name.'" id="'.$name.'" value="'.$val['value'].'"';
				if($val['value']==$value){
					$html.=' checked="checked" ';
				}
				$html.='/>
				'.$val['title'].'</label>
				';
			}
		}
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 多选
     * @param string $data['name'] name属性
     * @param string $data['list'] 二位数组array(array('title'=>'','value'=>''));
     * @param string $data['value'] 选中值多个请用,分割
     * @param string $data['datatype'] 验证规则
     * @param string $data['error'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function checkbox($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['list'])){
			$list=$data['list'];
		}else{
			return;
		}
		if(!empty($data['value'])){
			$value=explode(',',$data['value']);
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}
		$html='';
		if(!empty($list)||is_array($list)){
			foreach ($list as $val) {
				$html.='<label class="u-opt" '.$datatype.$errormsg.' >
				<input type="checkbox" name="'.$name.'[]" id="'.$name.'[]" value="'.$val['value'].'"';
				if(in_array($val['value'], (array)$value)){
					$html.=' checked="checked" ';
				}
				$html.='/>
				'.$val['title'].'</label>
				';
			}
		}
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 下拉菜单
     * @param string $data['name'] name属性
     * @param string $data['preset'] 显示请选择..
     * @param string $data['list'] 二位数组array(array('title'=>'','value'=>''));
     * @param string $data['value'] 选中值
     * @param string $data['datatype'] 验证规则
     * @param string $data['error'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function select($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['list'])){
			$list=$data['list'];
		}else{
			return;
		}
		if(!empty($data['value'])){
			$value=explode(',',$data['value']);
		}
		if(!empty($data['datatype'])){
			$datatype='datatype="'.$data['datatype'].'" ';
		}
		if(!empty($data['errormsg'])){
			$errormsg='errormsg="'.$data['errormsg'].'" ';
		}
		$html='<select class="u-slt" id="'.$name.'" name="'.$name.'">';
		if($data['preset']){
			$html.='<option>==选择'.$data['title'].'==</option>';
		}
		if(!empty($list)||is_array($list)){
			foreach ($list as $val) {
				$html.='
				<option value="'.$val['value'].'"';
				if($val['value']==$value){
					$html.=' selected="selected" ';
				}
				$html.='/>
				'.$val['title'].'</option>';
			}
		}
		$html.='</select>';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 图片上传
     * @param string $data['title'] 标题
     * @param string $data['tip'] 默认提示信息
     * @param string $data['name'] name属性
     * @param string $data['value'] 默认值
     * @param string $data['size'] 文本框大小1~6
     * @param string $data['css'] css样式
     * @param string $data['maxlength'] 最大字符数
     * @param string $data['datatype'] 验证规则
     * @param string $data['errormsg'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function imgUpload($data,$layout=true)
	{
		$txtHtml=self::text($data,false);
		$html=$txtHtml.'
		<button class="u-btn u-img-upload" type="button" data="'.$data['name'].'" preview="'.$data['name'].'_preview" id="'.$data['name'].'_button">上传</button>
		<button class="u-btn" type="button" id="'.$data['name'].'_preview">预览</button>
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 文件上传
     * @param string $data['title'] 标题
     * @param string $data['tip'] 默认提示信息
     * @param string $data['name'] name属性
     * @param string $data['value'] 默认值
     * @param string $data['size'] 文本框大小1~6
     * @param string $data['css'] css样式
     * @param string $data['maxlength'] 最大字符数
     * @param string $data['datatype'] 验证规则
     * @param string $data['errormsg'] 错误提示
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function fileUpload($data,$layout=true)
	{
		$txtHtml=self::text($data,false);
		$html=$txtHtml.'
		<button class="u-btn u-img-upload" type="button" data="'.$data['name'].'" id="'.$data['name'].'_button">上传</button>
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 多图片上传按钮
     * @param string $data['name'] 表单name值
     * @param string $data['value'] 文件ID可选，逗号分割
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function imagesUpload($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['value'])){
			$value=$data['value'];
		}
		$html='
		<button class="u-btn u-multi-upload" type="button" data="'.$name.'" id="'.$name.'_button">上传</button>
		<span class="suffix">上传后可拖动图片进行排序</span>
		<div class="m-multi-image f-cb" id="'.$name.'" data="'.$value.'"></div>
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 地区联动
     * @param string $data['name'] 表单name值
     * @param string $data['value'] 字段值
     * @param bool $layout 是否进行布局
     * @return string html字符串
     */
	public static function area($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['value'])){
			$value=$data['value'];
		}
		$html='
		<span class="u-area" data="'.$data['value'].'"> 
			<select class="u-slt" name="'.$name.'[province]">
			<option value="0">==省份==</option>
			</select>
			<select class="u-slt" name="'.$name.'[city]">
			<option value="0">==城市==</option>
			</select>
			<select class="u-slt" name="'.$name.'[county]">
			<option value="0">==区县==</option>
			</select>
		</span>
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 编辑器
     * @param string $data['name'] 文本框name值
     * @param string $data['content'] 默认内容
     * @param string $data['width'] 编辑器宽度
     * @param string $data['height'] 编辑器高度
     * @return string html字符串
     */
	public static function editor($data,$layout=true)
	{
		if(!empty($data['name'])){
			$name=$data['name'];
		}
		if(!empty($data['value'])){
			$content=$data['value'];
		}
		if(!empty($data['width'])){
			$width=$data['width'];
		}else{
			$width='100%';
		}
		if(!empty($data['height'])){
			$height=$data['height'];
		}else{
			$height='400px';
		}
		$html='
		<textarea name="'.$name.'" id="'.$name.'" class="u-editor" style="width:'.$width.';height:'.$height.';visibility:hidden;">'.$content.'</textarea>
		';
		if($layout){
			$html=self::layout($html,$data['title'],$data['tip']);
			self::$elementArray[]=$html;
		}
		return $html;
	}
	/**
     * 表单元素验证
     * @param string $data 表单值
     * @param string $datatype 验证规则
     * @return string html字符串
     */
	public static function check($data,$datatype)
	{
		if(empty($datatype)){
			return true;
		}
		//基本规则验证
		$datatype=trim($datatype);
		$head=substr($datatype, 0,1);
		if($head=='/'){
			if(preg_match($datatype,$data)){
				return true;
			}else{
				return false;
			}
		}
		//获取预设段
		$limit=substr($datatype,1,0);
		$limit=str_replace('-', ',', $limit);
		//预设规则验证
		switch ($head) {
			case '*':
				if(empty($limit)){
					$pattern='/[\w\W]+/';
				}else{
					$pattern='/^[\w\W]{'.$limit.'}$/';
				}
				break;
			case 'n':
				if(empty($limit)){
					$pattern='/^\d+$/';
				}else{
					$pattern='/^\d{'.$limit.'}$/';
				}
				break;
			case 's':
				if(empty($limit)){
					$pattern='/^[\x7f-\xff\d\w\.]+$/';
				}else{
					$pattern='/^[\x7f-\xff\d\w\.]{'.$limit.'}$/';
				}
				break;
			case 'p':
				$pattern='/^[1-9]\d{5}$/';
				break;
			case 'm':
				$pattern='/^1[34578][0-9]{9}$/';
				break;
			case 'e':
				$pattern="/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/";
				break;
			case 'u':
				if($datatype=='url'){
					$pattern='#^(http|https|ftp|ftps)://([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?#i';
				}
				break;
		}
		if(empty($pattern)&&!empty($datatype)){
			return false;
		}
		if(preg_match($pattern,$data)){
			return true;
		}else{
			return false;
		}
	}
}
?>