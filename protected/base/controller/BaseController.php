<?php
class BaseController extends Controller
{
    protected $appConfig = array();
    protected $siteConfig = array();
    public function __construct()
    {
        $this->appConfig = config('APP');
        if ($this->_readHtmlCache()) {
            $this->appConfig['HTML_CACHE_ON'] = false;
            die;
        }
        if ($_REQUEST['session_id']) {
            session_id($_REQUEST['session_id']);
        }
        @session_start();
        parent::__construct();
        if (!file_exists((ROOT_PATH . 'cache/install.lock')) && APP_NAME != 'install') {
            $this->redirect(url('install/index/index'));
        }
        if(file_exists(ROOT_PATH . 'cache/install.lock')) {
            $this->site();
        }
    }
    public function __destruct()
    {
        $this->_writeHtmlCache();
    }

    public function site()
    {
        $info = api('duxcms','setSiteConfig');
        $this->siteConfig=$info['data'];
        $_POST['site']=$info['siteId'];
        defined('SITEID') or define('SITEID', $info['siteId']);
        defined('OLDSITEID') or define('OLDSITEID', $info['data']['site_id']);
        
    }
    //读取静态缓存
    private function _readHtmlCache()
    {
        if ($this->appConfig['HTML_CACHE_ON'] == false || empty($this->appConfig['HTML_CACHE_RULE'])) {
            $this->appConfig['HTML_CACHE_ON'] = false;
            return false;
        }
        if (isset($this->appConfig['HTML_CACHE_RULE'][APP_NAME][CONTROLLER_NAME][ACTION_NAME])) {
            $expire = $this->appConfig['HTML_CACHE_RULE'][APP_NAME][CONTROLLER_NAME][ACTION_NAME];
        } else {
            if (isset($this->appConfig['HTML_CACHE_RULE'][APP_NAME][CONTROLLER_NAME]['*'])) {
                $expire = $this->appConfig['HTML_CACHE_RULE'][APP_NAME][CONTROLLER_NAME]['*'];
            } else {
                $this->appConfig['HTML_CACHE_ON'] = false;
                return false;
            }
        }
        return cpHtmlCache::read($this->appConfig['HTML_CACHE_PATH'], $expire);
    }
    //写入静态页面缓存
    private function _writeHtmlCache()
    {
        if ($this->appConfig['HTML_CACHE_ON']) {
            cpHtmlCache::write();
        }
    }
}