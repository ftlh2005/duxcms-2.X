<?php
/**
 * FormFieldModel.php
 * 表单字段表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FormFieldModel extends BaseModel
{
    protected $table = 'form_field';
    /**
     * 获取表单字段列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 表单字段列表
     */
    public function loadData($condition = null)
    {
        return $this->select($condition, '', 'sequence ASC,field_id ASC');
    }
    /**
     * 获取表单字段总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 表单字段信息
     * @param int $fieldId 表单字段ID
     * @return array 用户信息
     */
    public function getInfo($fieldId)
    {
        return $this->find('field_id=' . $fieldId);
    }
    /**
     * 添加表单字段信息
     * @param array $data 表单字段信息
     * @return int 表单字段ID
     */
    public function addData($data)
    {
        $formInfo=model('Form')->getInfo($data['form_id']);
        //获取表单字段属性
        $typeField = model('Field')->typeField();
        $propertyField = model('Field')->propertyField();
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //添加真实表单字段
        $sql="
        ALTER TABLE {$this->model->pre}form_{$formInfo['table']} ADD {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']}) DEFAULT NULL
        ";
        $this->query($sql);
        //格式化部分表单字段
        $data=model('Field')->foramtData($data);
        //插入数据
        return $this->insert($data);
    }
    /**
     * 保存表单字段信息
     * @param array $data 表单字段信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        $info=$this->getInfo($data['field_id']);
        $formInfo=model('Form')->getInfo($data['form_id']);
        //获取表单字段属性
        $typeField = model('Field')->typeField();
        $propertyField = model('Field')->propertyField();
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //修改真实表单字段
        if($info['type']<>$data['type']||$info['field']<>$data['field']){
            $sql="
            ALTER TABLE {$this->model->pre}form_{$formInfo['table']} CHANGE {$info['field']} {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']})
            ";
            $this->query($sql);
        }
        //格式化部分表单字段
        $data=model('Field')->foramtData($data);
        //修改数据
        return $this->update('field_id=' . $data['field_id'], $data);
    }
    /**
     * 删除表单字段信息
     * @param int $fieldId 表单字段ID
     * @return bool 状态
     */
    public function delData($fieldId)
    {
        //删除表
        $info=$this->getInfo($fieldId);
        $formInfo=model('Form')->getInfo($info['form_id']);
        $sql="
             ALTER TABLE {$this->model->pre}form_{$formInfo['table']} DROP {$info['field']}
            ";
        $this->model->query($sql);
        return $this->delete('field_id=' . $fieldId);
    }
    /**
     * 删除表单字段信息通过表单集
     * @param int $fieldId 表单ID
     * @return bool 状态
     */
    public function delDataForm($formId)
    {
        return $this->delete('form_id=' . $formId);
    }
    /**
     * 查找重复信息
     * @param string $field 字段名
     * @param string $formId 表单ID
     * @param string $fieldId 排除ID
     * @return array 用户信息
     */
    public function getInfoRepeat($field, $formId, $fieldId = null)
    {
        if ($fieldId) {
            return $this->count(('`field`="' . $field) . '" AND field_id<>' . $fieldId .' AND form_id='.$formId);
        } else {
            return $this->count(('`field`="' . $field) . '" AND form_id='.$formId);
        }
    }
    /**
     * 字段前台显示
     * @param int $type 字段类型
     * @return array 字段类型列表
     */
    public function showField($data,$type,$config)
    {
        switch ($type) {
            case '2':
            case '3':
                return html_out($data);
            case '6':
                //文件列表
                if(empty($data)){
                    return array();
                }
                $list=api('admin','getFileList',array('file_id'=>$data));
                return $list;
                break;
            case '7':
            case '8':
                if(empty($config)){
                    return $data;
                }
                $list=explode(",",trim($config));
                foreach ($list as $key => $vo) {
                    if($data==intval($key)+1){
                        return $vo;
                    }
                }
                break;
            case '9':
                if(empty($config)){
                    return $data;
                }
                $list=explode(",",trim($config));
                $dataArray=array();
                foreach ($list as $key => $vo) {
                    if($data==intval($key)){
                        $dataArray[]=$vo;
                    }
                }
                return $dataArray;
                break;
            default:
                return in($data);
                break;
        }
    }
}