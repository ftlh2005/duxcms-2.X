<?php
/**
 * SiteModel.php
 * 站点表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class SiteModel extends BaseModel
{
    protected $table = 'site';
    /**
     * 获取站点列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 碎片列表
     */
    public function loadData($condition = null, $limit = null)
    {
        return $this->select($condition, '', 'site_id ASC', $limit);
    }
    /**
     * 获取站点总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 站点信息
     * @param int $siteId 站点ID
     * @return array 用户信息
     */
    public function getInfo($siteId)
    {
        return $this->find('site_id=' . $siteId);
    }
    /**
     * 站点信息
     * @param int $siteId 站点ID
     * @return array 用户信息
     */
    public function getInfoDomain($siteDomain)
    {
        return $this->find('domain="' . $siteDomain .'"');
    }
    /**
     * 添加站点信息
     * @param array $data 站点信息
     * @return int 站点ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存站点信息
     * @param array $data 站点信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('site_id=' . $data['site_id'], $data);
    }
    /**
     * 删除站点信息
     * @param int $siteId 站点ID
     * @return bool 状态
     */
    public function delData($siteId)
    {
        return $this->delete('site_id=' . $siteId);
    }

    
}