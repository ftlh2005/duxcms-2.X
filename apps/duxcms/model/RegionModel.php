<?php
/**
 * RegionModel.php
 * 地区操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class RegionModel extends BaseModel
{
    protected $table = 'region';
    /**
     * 获取地区列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 地区列表
     */
    public function loadData($condition = null, $limit = null)
    {
        return $this->select($condition, '', 'region_id ASC', $limit);
    }
    /**
     * 获取地区显示
     * @param string $$data 数据
     * @param int $limit 数量
     * @return array 地区列表
     */
    public function getName($data)
    {
        $list = $this->select('region_id in('.$data.')');
        $name = '';
        if(!empty($list)){
            foreach ($list as $value) {
                $name.= $value['region_name'].'/';
            }
        }
        return substr($name, 0, -1);
    }
}