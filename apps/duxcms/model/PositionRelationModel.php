<?php
/**
 * PositionRelationModel.php
 * 推荐内容表操作
 * @author Life <349865361@qq.com>
 * @version 20140126
 */
class PositionRelationModel extends BaseModel
{
    protected $table = 'position_relation';
    /**
     * 获取推荐内容列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 推荐表列表
     */
    public function loadData($condition = null, $limit = 20, $extTable=null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $field = ' B.*,C.name as cname,C.app,C.urlname as urlname,A.data_id,A.content_id,A.image as pos_image,A.title as pos_title, A.content , A.sequence, D.name as pos_name';
        $obj = $this->model->field($field)
                        ->table('position_relation','A')
                        ->Join('content','B',array('A.content_id','B.content_id'))
                        ->Join('category','C',array('B.class_id','C.class_id'))
                        ->Join('position','D',array('A.pos_id','D.pos_id'));
        //扩展字段表
        if(!empty($extTable)){
            $obj=$obj->field('E.*'.','.$field)
                    ->leftJoin('fieldset_'.$extTable, 'E', array( 'A.content_id', 'E.content_id' ));
        }
        $list=$obj->where($condition)
                        ->limit($limit)
                        ->order('A.sequence ASC, B.sequence ASC,B.time DESC, B.content_id DESC')
                        ->select();
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key] = $value;
                if(!empty($data[$key]['pos_title'])){
                    $data[$key]['title']=$data[$key]['pos_title'];
                }
                if(!empty($data[$key]['pos_image'])){
                    $data[$key]['image']=$data[$key]['pos_image'];
                }
                $data[$key]['content']=$data[$key]['content'];
            }
        }
        return $data;
    }
    /**
     * 获取TA内容总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model
                        ->table('position_relation','A')
                        ->Join('content','B',array('A.content_id','B.content_id'))
                        ->Join('category','C',array('B.class_id','C.class_id'))
                        ->Join('position','D',array('A.pos_id','D.pos_id'))
                        ->where($condition)
                        ->count();
    }
    /**
     * 推荐内容信息
     * @param int $dataId 推荐内容ID
     * @return array 用户信息
     */
    public function getInfo($dataId)
    {
        return $this->find('site = '.SITEID.' AND data_id=' . $dataId);
    }
    /**
     * 添加推荐内容信息
     * @param array $data 推荐内容信息
     * @return int 推荐内容ID
     */
    public function addDataContent($data)
    {
        //获取推荐内容
        $info=$this->count('site = '.SITEID.' AND content_id=' . $data['content_id'].' AND pos_id=' . $data['pos_id']);
        if(!$info){
            $data['site'] = SITEID;
            return $this->insert($data);
        }else{
            return false;
        }
    }
    /**
     * 添加推荐信息
     * @param array $data 推荐内容信息
     * @return int 推荐内容ID
     */
    public function addData($data)
    {
        $data['site'] = SITEID;
        return $this->insert($data);
    }
    /**
     * 保存推荐内容信息
     * @param array $data 推荐内容信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        $data['start_time'] = strtotime($data['start_time']);
        $data['stop_time'] = strtotime($data['stop_time']);
        return $this->update('site = '.SITEID.' AND data_id=' . $data['data_id'], $data);
    }
    /**
     * 删除推荐内容信息
     * @param int $dataId 推荐内容ID
     * @return bool 状态
     */
    public function delData($dataId)
    {
        return $this->delete('site = '.SITEID.' AND data_id=' . $dataId);
    }
    /**
     * 推荐位内容列表标签
     * @param array $data 标签信息
     * @return array TAG列表
     */
    public function loadLabelList($data)
    {
        $where=array();
        if(empty($data['posId'])){
            return array();
        }else{
            $where['posId']=' AND A.pos_id in(' . $data['posId'] . ')';
        }
        if(!empty($data['classId'])){
            $where['classId']=' AND C.class_id in(' . $data['classId'] . ')';
        }
        if ($data['type']=='sub'&&!empty($data['classId'])) {
            $catalogInfo=api('duxcms','getCatalogClass',array('class_id'=>$data['classId']));
            $where['classId'] = " AND C.class_id in (" . api('duxcms','getCatalogSubClass',array('catalog_id'=>$catalogInfo['catalog_id'])) .")";
        }
        if (isset($data['image'])) {
            if($data['image'] == true)
            {
                $where['image'] = ' AND B.image<>"" ';
            }else{
                $where['image'] = ' AND B.image = "" ';
            }
        }
        if (!empty($data['where'])) {
            $where['where'] = ' AND '.$data['where'];
        }
        if (empty($data['limit'])) {
            $data['limit'] = 10;
        }
        $where['time']= ' AND A.start_time < '.time().' AND A.stop_time > '.time();
        $where=implode(' ', $where);
        $list=$this->loadData('A.status AND B.status=1'.$where,$data['limit'],$data['extTable']);
        if(empty($list)){
            return array();
        }
        $appConfig=config('APP');
        $data=array();
        foreach ($list as $key => $value) {
            $data[$key]=$value;
            $data[$key]['curl']=api($value['app'],'getCurl',array('data'=>$value,'config'=>$appConfig));
            $data[$key]['aurl']=api($value['app'],'getAurl',array('data'=>$value,'config'=>$appConfig));
        }
        return $data;
    }
}