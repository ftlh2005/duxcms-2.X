<?php
/**
 * CatalogModel.php
 * 菜单表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class CatalogModel extends BaseModel
{
    protected $table = 'catalog';
    /**
     * 获取菜单表列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 菜单表列表
     */
    public function loadData($condition = null, $limit=null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model
                    ->field(' A.*,B.name as class_name, B.urlname,B.app')
                    ->table('catalog', 'A')
                    ->leftJoin('category', 'B', array('A.class_id','B.class_id'))
                    ->where($condition)
                    ->limit($limit)
                    ->order('sequence ASC,catalog_id ASC')
                    ->select();
    }
    /**
     * 获取菜单分类数
     * @param array $list 条件
     * @return array 菜单树列表
     */
    public function getTrueData($condition = null, $catalog_id=0)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND ';
        }else{
            $condition='A.site = '.SITEID;
        }
        $data=$this->model
                    ->field('A.*,B.name as class_name, B.urlname,B.app')
                    ->table('catalog', 'A')
                    ->leftJoin('category', 'B', array('A.class_id','B.class_id'))
                    ->where($condition)
                    ->order('sequence ASC,catalog_id ASC')
                    ->select();

        $cat = new Category(array('catalog_id', 'parent_id', 'tree', 'tree'));
        $data=$cat->getTree($data, intval($catalog_id));
        return $data;
    }
    /**
     * 获取菜单表总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND ';
        }else{
            $condition='site = '.SITEID;
        }
        return $this->count($condition);
    }
    /**
     * 菜单表信息
     * @param int $catalog 菜单表ID
     * @return array 用户信息
     */
    public function getInfo($catalog)
    {
        return $this->find('site = '.SITEID.' AND catalog_id=' . $catalog);
    }
    /**
     * 菜单信息通过栏目
     * @param int $classId 栏目ID
     * @return array 用户信息
     */
    public function getInfoClass($classId,$catalogId=null)
    {
        if($catalogId){
            return $this->find('site = '.SITEID.' AND class_id=' . $classId.' AND catalog_id<>'.$catalogId);
        }else{
            return $this->find('site = '.SITEID.' AND class_id=' . $classId);
        }
    }
    /**
     * 添加菜单表信息
     * @param array $data 菜单表信息
     * @return int 菜单表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 栏目菜单添加
     * @param array $data 菜单表信息
     * @return int 菜单表ID
     */
    public function addDataClass($data)
    {
        $menuData=array();
        if($data['parent_id']){
            $parentInfo=$this->getInfoClass($data['parent_id']);
            $menuData['parent_id']=intval($parentInfo['catalog_id']);
        }else{
            $menuData['parent_id']=0;
        }
        $menuData['site'] = SITEID;
        $menuData['class_id']=$data['class_id'];
        return $this->insert($menuData);
    }
    /**
     * 保存菜单表信息
     * @param array $data 菜单表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->update('site = '.SITEID.' AND catalog_id=' . $data['catalog_id'], $data);
    }

    /**
     * 栏目菜单编辑
     * @param array $data 菜单表信息
     * @return int 菜单表ID
     */
    public function editDataClass($data)
    {
        $menuData=array();
        if($data['parent_id']){
            $parentInfo=$this->getInfoClass($data['parent_id']);
            $menuData['parent_id']=intval($parentInfo['catalog_id']);
        }else{
            $menuData['parent_id']=0;
        }
        return $this->update('site = '.SITEID.' AND class_id='.$data['class_id'],$menuData);
    }
    /**
     * 删除菜单表信息
     * @param int $catalog 菜单表ID
     * @return bool 状态
     */
    public function delData($catalog)
    {
        //删除关联内容
        return $this->delete('site = '.SITEID.' AND catalog_id=' . $catalog);
    }
    /**
     * 栏目菜单删除
     * @param int $classId 栏目ID
     * @return bool 状态
     */
    public function delDataClass($classId)
    {
        //删除关联内容
        return $this->delete('site = '.SITEID.' AND class_id=' . $classId);
    }
    /**
     * 数据格式化
     * @param array $data 菜单信息
     * @return array 格式化后信息
     */
    public function foramtData($data)
    {
        $data['site'] = SITEID;
        return $data;
    }
    /**
     * 菜单列表标签
     * @param array $data 标签信息
     * @return array 菜单列表
     */
    public function loadLabelList($data)
    {
        $where='';
        if(isset($data['parentId'])){
            $where='AND A.parent_id='.$data['parentId'];
        }
        if(isset($data['catalogId'])){
            $where='AND A.catalog_id in('.$data['parentId'].')';
        }
        if(isset($data['classId'])){
            $where='AND A.class_id in('.$data['classId'].')';
        }
        if(empty($data['limit'])){
            $data['limit']=10;
        }
        $list=$this->loadData('A.show=1 '.$where,$data['limit']);
        if(empty($list)){
            return array();
        }
        return $this->formatUrlArray($list);
    }
    /**
     * 获取菜单面包屑
     * @param int $catalogId 菜单ID
     * @return array 菜单表列表
     */
    public function loadCrumb($catalogId)
    {
        $data=$this->loadData();
        if(empty($data)){
             return array();
        }
        $cat = new Category(array(
            'catalog_id',
            'parent_id',
            'name',
            'class_name',
            'url'
            ));
        $data=$cat->getPath($data, $catalogId);
        return $this->formatUrlArray($data);    
    }
    /**
     * 格式化URL数组
     * @param array $catalogId 菜单ID
     * @return array 菜单表列表
     */
    public function formatUrlArray($data)
    {
        $config=config('APP');
        $list=array();
        foreach ($data as $key => $value) {
            $list[$key]=$value;
            if(empty($value['name'])){
                $list[$key]['name']=$value['class_name'];
            }
            if(!empty($value['url'])){
                $list[$key]['url']=$value['url'];
            }else{
                $list[$key]['url']=$this->getCurl($value,$config);
            }
            unset($list[$key]['app']);
            unset($list[$key]['show']);
            unset($list[$key]['sequence']);
            unset($list[$key]['class_name']);
            unset($list[$key]['urlname']);
        }
        return $list;
    }

    /**
     * 获取菜单栏目URL
     * @param array $data 栏目数据
     * @param array $config APP配置信息
     * @return string 栏目链接
     */
    public function getCurl($data,$config)
    {
        if($config['URL_REWRITE_ON']){
            $rewrite = config('REWRITE');
            $rewrite = array_flip($rewrite);
            $url=$rewrite[$data['app'].'/Category/index'];
            $parameter=array('class_id'=>$data['class_id']);
            if(!empty($url)){
                $parameter=array();
                if(strpos($url,'<class_id>')){
                    $parameter['class_id']=$data['class_id'];
                }
                if(strpos($url,'<urlname>')){
                    $parameter['urlname']=$data['urlname'];
                }
            }
        }else{
            $parameter=array('class_id'=>$data['class_id']);
        }
        $url = url($data['app'].'/Category/index',$parameter);
        return urldecode($url);
    }
     /**
     * 获取子菜单栏目
     * @param array $catalogId 菜单ID
     * @return array 菜单表列表
     */
    public function getSubClass($catalogId)
    {
        $data = $this->getTrueData('', $catalogId);
        if(empty($data)){
            return;
        }
        $list=array();
        foreach ($data as $value) {
            if(empty($value['url'])){
                $list[]=$value['class_id'];
            }
        }
        return implode(',', $list);
        
    }
}