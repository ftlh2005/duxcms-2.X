<?php
/**
 * FieldDataModel.php
 * 字段扩展扩展表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FieldDataModel extends BaseModel
{
    protected $table = 'fieldset_data';
    /**
     * 字段扩展信息
     * @param int $table 扩展表信息
     * @param int $fieldId 字段扩展ID
     * @return array 用户信息
     */
    public function getInfo($table,$fieldId)
    {
        $this->table='fieldset_'.$table;
        return $this->find('field_id=' . $fieldId);
    }
    /**
     * 字段扩展信息
     * @param int $table 扩展表信息
     * @param int $contentId 内容ID
     * @return array 用户信息
     */
    public function getInfoContent($table,$contentId)
    {
        $this->table='fieldset_'.$table;
        return $this->find('content_id=' . $contentId);
    }
    /**
     * 添加扩展内容
     * @param array $data 字段信息
     * @return int 字段ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        if(empty($data)){
            return;
        }
        return $this->insert($data);
    }
    /**
     * 修改扩展内容
     * @param array $data 字段信息
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        if(empty($data)){
            return;
        }
        if($this->count('content_id ='.$data['content_id'])){
            return $this->update('content_id ='.$data['content_id'],$data);
        }else{
            return $this->insert($data);
        }
    }
    /**
     * 删除扩展内容
     * @param int $fieldId 字段ID
     * @return bool 状态
     */
    public function delData($contentId)
    {
        //获取表信息
        $contentInfo=model('Content')->getInfo($contentId);
        if(empty($contentInfo)){
            return;
        }
        $classInfo=model('Category')->getInfo($contentInfo['class_id']);
        if(empty($classInfo)||!$classInfo['fieldset_id']){
            return;
        }
        $fieldsetInfo=model('Fieldset')->getInfo($classInfo['fieldset_id']);
        if(empty($fieldsetInfo)){
            return;
        }
        //删除内容
        $this->table='fieldset_'.$fieldsetInfo['table'];
        return $this->delete('content_id in(' . $contentId .')');
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        if(empty($data['class_id'])){
            return;
        }
        //获取信息
        $classInfo=model('Category')->getInfo($data['class_id']);
        if(empty($classInfo)||!$classInfo['fieldset_id']){
            return;
        }
        $fieldsetInfo=model('Fieldset')->getInfo($classInfo['fieldset_id']);
        if(empty($fieldsetInfo)){
            return;
        }
        $this->table='fieldset_'.$fieldsetInfo['table'];
        $fieldList=model('Field')->loadData('fieldset_id='.$classInfo['fieldset_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        $newData=array();
        $newData['content_id']=$data['content_id'];
        foreach ($fieldList as $value) {
            $newData[$value['field']]=model('Field')->formatField($data['Fieldset_'.$value['field']],$value['type']);
        }
        return $newData;
    }

}