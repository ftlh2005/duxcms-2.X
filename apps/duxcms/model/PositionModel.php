<?php
/**
 * PositionModel.php
 * 推荐表操作
 * @author Life <349865361@qq.com>
 * @version 20140126
 */
class PositionModel extends BaseModel
{
    protected $table = 'position';
    /**
     * 获取推荐列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 推荐列表
     */
    public function loadData($condition = null, $limit = '')
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->select($condition, '', 'pos_id ASC', $limit);
    }
    /**
     * 获取推荐总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->count($condition);
    }
    /**
     * 推荐信息
     * @param int $posId 推荐ID
     * @return array 用户信息
     */
    public function getInfo($posId)
    {
        return $this->find('site = '.SITEID.' AND pos_id=' . $posId);
    }
    /**
     * 添加推荐信息
     * @param array $data 推荐信息
     * @return int 推荐ID
     */
    public function addData($data)
    {
        $data['site'] = SITEID;
        return $this->insert($data);
    }
    /**
     * 保存推荐信息
     * @param array $data 推荐信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('site = '.SITEID.' AND pos_id=' . $data['pos_id'], $data);
    }
    /**
     * 删除推荐信息
     * @param int $posId 推荐ID
     * @return bool 状态
     */
    public function delData($posId)
    {
        return $this->delete('site = '.SITEID.' AND pos_id=' . $posId);
    }
}