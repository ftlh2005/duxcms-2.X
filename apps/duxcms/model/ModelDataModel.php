<?php
/**
 * ModelDataModel.php
 * 内容模型操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class ModelDataModel extends BaseModel
{
    /**
     * 获取模型Api列表
     * @return array 模型列表
     */
    public function loadData()
    {
        $list=hook_api('apiContentModel');
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['app']=$key;
            }
        }
        return array_order($data, 'order', 'asc');
        return $data;
    }
    /**
     * 获取模型名称
     * @return array 模型列表名称
     */
    public function getName()
    {
        $list=hook_api('apiContentModel');
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value['name'];
            }
        }
        return $data;
    }
}