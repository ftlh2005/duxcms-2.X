<?php
/**
 * TagsRelationModel.php
 * TAG关系表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class TagsRelationModel extends BaseModel
{
    protected $table = 'tags_relation';
    /**
     * 获取TAG管理内容列表
     * @param int $contentId 内容ID
     * @return array TAG表列表
     */
    public function loadDataContent($contentId)
    {
        return $this->select('content_id in('.$contentId.')', '', 'tag_id DESC');
    }
    /**
     * 获取TAG内容列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array TAG表列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->model->field(' distinct  B.*,C.name as cname,C.app,C.urlname as urlname')
                        ->table('tags_relation','A')
                        ->Join('content','B',array('A.content_id','B.content_id'))
                        ->Join('category','C',array('B.class_id','C.class_id'))
                        ->Join('tags','D',array('A.tag_id','D.tag_id'))
                        ->where($condition)
                        ->limit($limit)
                        ->order('A.tag_id DESC')
                        ->select();
    }
    /**
     * 获取TA内容总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->model->table('tags_relation','A')
                        ->field(' distinct ')
                        ->Join('content','B',array('A.content_id','B.content_id'))
                        ->Join('category','C',array('B.class_id','C.class_id'))
                        ->Join('tags','D',array('A.tag_id','D.tag_id'))
                        ->where($condition)
                        ->count();
    }
    /**
     * TAG信息
     * @param string $where 条件
     * @return array 用户信息
     */
    public function getInfo($where)
    {
        return $this->find($where);
    }
    /**
     * 添加TAG关系表信息
     * @param array $data TAG关系表信息
     * @return int TAG关系表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 删除TAG关系表信息
     * @param int $tagsId TAG关系表ID
     * @return bool 状态
     */
    public function delData($tagId)
    {
        //删除关联内容
        return $this->delete('tag_id in(' . $tagId . ')');
    }
    /**
     * 数据格式化
     * @param array $data TAG信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        return $data;
    }
    /**
     * 删除TAG内容关系信息
     * @param int $contentId 内容表ID
     * @return bool 状态
     */
    public function delDataContent($contentId)
    {
        //删除关联内容
        return $this->delete('content_id in(' . $contentId . ')');
    }
    /**
     * TAG内容列表标签
     * @param array $data 标签信息
     * @return array TAG列表
     */
    public function loadLabelList($data)
    {
        if(!empty($data['order'])){
            $data['order']=$data['order'].',';
        }
        $where='';
        if(!empty($data['name'])){
            $name=explode(',', $data['name']);
            foreach ($name as $value) {
                $where.=' OR D.name="'.$value.'" ';
            }
        }
        if (isset($data['image'])) {
            if($data['image'] == true)
            {
                $where.= ' AND B.image<>"" ';
            }else{
                $where.= ' AND B.image = "" ';
            }
        }
        if(empty($data['limit'])){
            $data['limit']=10;
        }
        if(!empty($where)){
            $where = substr($where, 4);
        }
        $list = $this->loadData($where, $data['limit'],$data['order']);
        $data=array();
        if(!empty($list)){
            $appConfig=config('APP');
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['curl']=api($value['app'],'getCurl',array('data'=>$value,'config'=>$appConfig));
                $data[$key]['aurl']=api($value['app'],'getAurl',array('data'=>$value,'config'=>$appConfig));
            }
        }
        return $data;
    }
}