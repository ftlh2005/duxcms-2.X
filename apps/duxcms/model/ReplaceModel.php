<?php
/**
 * ReplaceModel.php
 * 替换表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class ReplaceModel extends BaseModel
{
    protected $table = 'replace';
    /**
     * 获取替换列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 替换列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->select($condition, '', 'replace_id ASC', $limit);
    }
    /**
     * 获取替换总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 替换信息
     * @param int $replaceId 替换ID
     * @return array 用户信息
     */
    public function getInfo($replaceId)
    {
        return $this->find('replace_id=' . $replaceId);
    }
    /**
     * 添加替换信息
     * @param array $data 替换信息
     * @return int 替换ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存替换信息
     * @param array $data 替换信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('replace_id=' . $data['replace_id'], $data);
    }
    /**
     * 删除替换信息
     * @param int $replaceId 替换ID
     * @return bool 状态
     */
    public function delData($replaceId)
    {
        return $this->delete('replace_id=' . $replaceId);
    }
    /**
     * 处理数据替换
     * @param string $data 数据内容
     * @return string 处理后数据
     */
    public function replaceData($data)
    {
        if(empty($data)){
            return $data;
        }
        $replace = $this->loadData('',100);
        $data=html_out($data);
        if (!empty($replace)) {
            foreach ($replace as $export) {
                if(!empty($export['key'])){
                    $data = preg_replace("/(?!<[^>]+)".preg_quote($export['key'],'/')."(?![^<]*>)/",$export['content'], $data,$export['num']);
                }
            }
        }
        return $data;
    }
}