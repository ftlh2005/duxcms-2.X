<?php
/**
 * SiteConfigModel.php
 * 站点配置表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class SiteConfigModel extends BaseModel
{
    protected $table = 'site_config';
    /**
     * 设置站点
     * @param int $siteId 站点配置ID
     * @return array 站点信息
     */
    public function setSite()
    {
        //查找域名站点
        $domain=trim($_SERVER['HTTP_HOST']);
        $hostInfo=model('Site')->getInfoDomain($domain);
        $siteId = $hostInfo['site_id'];
        //通过GET与cookie查找
        $getSite=intval($_GET['site']);
        $cookieSite = get_cookie('site');
        if(!empty($getSite)&&empty($siteId)){
            $siteId = $getSite;
        }else if(!empty($cookieSite)&&empty($siteId)){
            $siteId = $cookieSite;
        }else if(empty($siteId)){
            $siteId = 1;
        }
        $info = $this->getInfo($siteId);
        if(empty($info)){
            $info = $this->getInfo(1);
            $siteId = 1;
        }
        $siteInfo = model('Site')->getInfo($siteId);
        if($siteInfo['data_type']){
            $siteId = $siteInfo['data_type'];
        }
        //设置Cookie
        if(!empty($getSite)){
            set_cookie('site', $info['site_id'], '/',  31557600);
        }
        return array('siteId'=>$siteId,'data'=>$info);
    }
    /**
     * 站点配置信息
     * @param int $siteId 站点配置ID
     * @return array 用户信息
     */
    public function getInfo($siteId)
    {
        return $this->find('site_id=' . $siteId);
    }
    /**
     * 添加站点配置信息
     * @param array $data 站点配置信息
     * @return int 站点配置ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存站点配置信息
     * @param array $data 站点配置信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('site_id=' . $data['site_id'], $data);
    }
    /**
     * 删除站点配置信息
     * @param int $siteId 站点配置ID
     * @return bool 状态
     */
    public function delData($siteId)
    {
        return $this->delete('site_id=' . $siteId);
    }
}