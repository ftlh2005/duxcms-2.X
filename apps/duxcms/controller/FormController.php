<?php
/**
 * FormController.php
 * 表单列表
 * @author Life <349865361@qq.com>
 * @version 20140125
 */
class FormController extends SiteController
{
    /**
     * 表单内容列表
     */
    public function index()
    {
    	$table=urldecode($_GET['name']);
    	$table = msubstr(in($table),0,20);
    	//查询表单信息
    	$info = model('Form')->getInfoName($table);
    	if(empty($info)){
            $this->error404();
        }
        if(!$info['display']){
            $this->error404();
        }
        //分页参数
        $listRows = $info['page'];
        if(empty($listRows)){
            $listRows = 20;
        }
        $urlArray = array(
            'page' => '{page}',
            'name' => $table,
        );
        $url = url('Form/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //获取表单字段
        $fieldList=model('FormField')->loadData('form_id='.$info['form_id']);
        //获取表单提交html
        $formHtml=model('FormData')->getField($info['form_id'],'',false);
        $postKey=getcode(32);
        $formTime = form_time();
        $formHtml.='<input name="post_key" type="hidden" value="'.$postKey.'">';
        $formHtml.='<input name="form" type="hidden" value="'.$table.'">';
        $formHtml.='<input name="relation_key" type="hidden" value="'.$formTime.'">';
        $config=config('APP');
        $_SESSION[$config['COOKIE_PREFIX'].'form_'.$table]=$postKey;
        //菜单列表信息
        $list = model('FormData')->loadData($table, $info['where'], $info['sequence'], $limit);
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                foreach ($fieldList as $v) {
                    $data[$key][$v['field']]=model('FormField')->showField($value[$v['field']],$v['type'],$v['config'],t);
                }
            }
        }
        $count = model('FormData')->countData($table, $info['where']);
        //获取分页
        $page = $this->pageShow($count);
        //位置导航
        $crumb = array(
            0=>array('name'=>$info['name'],'url'=> url('Index/index',array('name'=>$table))),
            );
        //MEDIA信息
        $media = api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig,'title'=>$info['name'],'keywords'=>$info['name'],'description'=>$info['name']));
        //模板赋值
        $this->assign('pageList', $data);
        $this->assign('crumb', $crumb);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->assign('formHtml', $formHtml);
        $this->assign('postKey', $postKey);
        $this->assign('form', $table);
        $this->assign('formInfo', $info);
        $this->assign('relation_key', $formTime);
        $this->show($info['tpl']);
    }

    //提交表单
    public function post()
    {
        $table=in($_POST['form']);
        if(empty($table)){
            $this->error404();
        }
        $info=model('Form')->getInfoName($table);
        if(empty($info)||!$info['release']){
            $this->error404();
        }
        //判断安全码
        $config=config('APP');
        $sessionKey=$config['COOKIE_PREFIX'].'form_'.$table;
        if(empty($_POST['post_key'])||$_SESSION[$sessionKey]<>$_POST['post_key']){
            $this->msg('无法通过安全验证，请勿非法提交！',false);
        }
        unset($_SESSION[$config['COOKIE_PREFIX'].'form_'.$table]);
        //判断发布间隔
        $sessionTime=$config['COOKIE_PREFIX'].'form_'.$table.'_time';
        if(isset($_SESSION[$sessionTime]) && time()-$_SESSION[$sessionTime]<10){
            $this->msg('发布时间间隔为10秒，请稍后操作！',false);
        }
        $_SESSION[$sessionTime]=time();
        //获取字段列表
        $fieldList=model('FormField')->loadData('form_id='.$info['form_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            $this->msg('没有可提交的表单信息！',false);
        }
        $_POST['form_id']=$info['form_id'];
        $checkField=model('FormData')->checkField($_POST);
        if(!empty($checkField)){
            if(is_ajax()){
                $this->msg($checkField, false);
            }else{
                $this->alert($checkField);
            }
        }
        //录入数据
        model('FormData')->addData($_POST);
        if(is_ajax()){
            $this->msg($info['return_msg']);
        }else{
            $this->alert($info['return_msg'],$info['return_url']);
        }
    }

}