<?php
/**
 * AdminCategoryController.php
 * 内容分类基础
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminCategoryController extends AdminController
{
    /**
     * 编辑部分参数
     */
    public function edit()
    {
        $name=in($_POST['name']);
        $value=in($_POST['data']);
        $class_id=intval($_GET['class_id']);
        if(empty($class_id)){
            $this->msg('无法定位编辑数据！',false);
        }
        if(empty($name)){
            $this->msg('编辑数据无法获取！',false);
        }
        $data=array();
        $data[$name]=$value;
        $data['class_id']=$class_id;
        model('Category')->saveData($data);
        $this->msg($value);
    }
}