<?php
/**
 * AdminFieldsetController.php
 * 内容字段集管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminFieldsetController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminFieldset/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //字段集列表信息
        $list = model('Fieldset')->loadData($where, $limit);
        $count = model('Fieldset')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加字段集
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminfieldset/info');
    }
    /**
     * 处理字段集添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['table'])) {
            $this->msg('字段表不能为空！', false);
        }
        if(model('Fieldset')->getInfoRepeat($_POST['table'])){
            $this->msg('字段表不能重复！', false);
        }
        model('Fieldset')->addData($_POST);
        $this->msg('字段集添加成功！', 1);
    }
    /**
     * 编辑字段集资料
     */
    public function edit()
    {
        $fieldsetId = intval($_GET['fieldset_id']);
        if (empty($fieldsetId)) {
            $this->msg('无法获取字段集ID！', false);
        }
        //字段集信息
        $info = model('Fieldset')->getInfo($fieldsetId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminfieldset/info');
    }
    /**
     * 处理字段集资料
     */
    public function editData()
    {
        $fieldsetId = intval($_POST['fieldset_id']);
        if (empty($fieldsetId)) {
            $this->msg('无法获取字段集ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['table'])) {
            $this->msg('字段表不能为空！', false);
        }
        if(model('Fieldset')->getInfoRepeat($_POST['table'],$fieldsetId)){
            $this->msg('字段表不能重复！', false);
        }
        model('Fieldset')->saveData($_POST);
        $this->msg('字段集修改成功！', 1);
    }
    /**
     * 删除字段集
     */
    public function del()
    {
        $fieldsetId = intval($_POST['data']);
        if (empty($fieldsetId)) {
            $this->msg('字段集ID无法获取！', false);
        }
        model('Fieldset')->delData($fieldsetId);
        model('Field')->delDataFieldset($fieldsetId);
        $this->msg('字段集删除成功！');
    }
    /**
     * AJAX获取字段集列表
     */
    public function getField()
    {
        $classId=$_POST['class_id'];
        if(empty($classId)){
            return;
        }
        //获取栏目信息
        $classInfo=model('Category')->getInfo($classId);
        
        if(empty($classInfo)||!$classInfo['fieldset_id']){
            return;
        }

        //获取字段列表
        $fieldsetInfo=model('Fieldset')->getInfo($classInfo['fieldset_id']);
        if(empty($fieldsetInfo)){
            return;
        }
        $fieldList=model('Field')->loadData('fieldset_id='.$classInfo['fieldset_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        $typeField=model('Field')->typeField();
        //获取扩展内容信息
        $contentId=$_POST['content_id'];
        if(!empty($contentId)){
            $contentInfo=model('FieldData')->getInfoContent($fieldsetInfo['table'],$contentId);
        }
        $html='';
        $form=new DuxForm();
        foreach ($fieldList as $value) {
            $method=$typeField[$value['type']]['html'];
            $config=array();
            $config['title']=$value['name'];
            $config['name']='Fieldset_'.$value['field'];
            if($contentInfo[$value['field']]){
                $config['value']=$contentInfo[$value['field']];
            }else{
                $config['value']=$value['default'];
            }
            $config['size']=$value['size'];
            $config['datatype']=unserialize($value['datatype']);
            $config['tip']=$value['tip'];
            $config['errormsg']=$value['errormsg'];
            $list=explode(',', $value['config']);
            $newList=array();
            if(!empty($list)){
                foreach ($list as $key => $value) {
                    $newList[$key]['value']=$key+1;
                    $newList[$key]['title']=$value;
                }
            }
            $config['list']=$newList;
            $html.=$form->$method($config);
        }
        echo $html;
    }
}