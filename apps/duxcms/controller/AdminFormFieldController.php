<?php
/**
 * AdminFormFieldController.php
 * 内容表单管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminFormFieldController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'form_id' => $formId,
            'keyword' => $filterKeyword,
        );
        $url = url('AdminFormField/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'form_id='.$formId.$filterWhere;
        //表单列表信息
        $list = model('FormField')->loadData($where, $limit);
        $count = model('FormField')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('formInfo', model('Form')->getInfo($formId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加表单
     */
    public function add()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }

        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('formInfo', model('Form')->getInfo($formId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('propertyField', model('Field')->propertyField());
        $this->show('adminformfield/info');
    }
    /**
     * 处理表单添加
     */
    public function addData()
    {
        if (empty($_POST['form_id'])) {
            $this->msg('无法获取表单ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('FormField')->getInfoRepeat($_POST['field'],$_POST['form_id'])){
            $this->msg('字段不能重复！', false);
        }
        model('FormField')->addData($_POST);
        $this->msg('字段添加成功！', 1);
    }
    /**
     * 编辑表单资料
     */
    public function edit()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        $fieldId = intval($_GET['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        //表单信息
        $info = model('FormField')->getInfo($fieldId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('formInfo', model('Form')->getInfo($formId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('propertyField', model('Field')->propertyField());
        $this->show('adminformfield/info');
    }
    /**
     * 处理表单资料
     */
    public function editData()
    {
        $fieldId = intval($_POST['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        if (empty($_POST['form_id'])) {
            $this->msg('表单无法获取！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('FormField')->getInfoRepeat($_POST['field'],$_POST['form_id'],$fieldId)){
            $this->msg('字段不能重复！', false);
        }
        model('FormField')->saveData($_POST);
        $this->msg('字段修改成功！', 1);
    }
    /**
     * 删除表单
     */
    public function del()
    {
        $fieldId = intval($_POST['data']);
        if (empty($fieldId)) {
            $this->msg('字段ID无法获取！', false);
        }
        model('FormField')->delData($fieldId);
        $this->msg('字段删除成功！');
    }
}