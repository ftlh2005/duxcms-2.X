<?php
/**
 * AdminSiteManageController.php
 * 站点管理
 * @author Life <349865361@qq.com>
 * @version 20140125
 */
class AdminSiteManageController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' key LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminSiteManage/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //站点列表信息
        $list = model('Site')->loadData($where, $limit);
        $count = model('Site')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加站点
     */
    public function add()
    {
        //模板赋值
        $this->assign('siteList', model('Site')->loadData());
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminsitemanage/info');
    }
    /**
     * 处理站点添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('站点名称不能为空！', false);
        }
        $id = model('Site')->addData($_POST);
        $siteId = 1;
        if(!empty($_POST['data_type'])){
            $siteId = $_POST['data_type'];
        }
        $data = model('SiteConfig')->getInfo($siteId);
        $data['site_id'] = $id;
        model('SiteConfig')->addData($data);
        $this->msg('站点添加成功！', 1);
    }
    /**
     * 编辑站点资料
     */
    public function edit()
    {
        $siteId = intval($_GET['site_id']);
        if (empty($siteId)) {
            $this->msg('无法获取站点ID！', false);
        }
        //站点信息
        $info = model('Site')->getInfo($siteId);
        //模板赋值
        $this->assign('siteList', model('Site')->loadData());
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminsitemanage/info');
    }
    /**
     * 处理站点资料
     */
    public function editData()
    {
        $siteId = intval($_POST['site_id']);
        if (empty($siteId)) {
            $this->msg('无法获取站点ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('站点名称不能为空！', false);
        }
        model('Site')->saveData($_POST);
        $this->msg('站点修改成功！', 1);
    }
    /**
     * 删除站点
     */
    public function del()
    {
        $siteId = intval($_POST['data']);
        if (empty($siteId)) {
            $this->msg('站点ID无法获取！', false);
        }
        if($siteId == 1 ){
            $this->msg('保留站点无法删除！', false);
        }
        model('Site')->delData($siteId);
        model('SiteConfig')->delData($siteId);
        $this->msg('站点删除成功！');
    }
}