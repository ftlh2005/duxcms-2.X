<?php
/**
 * AdminFormDataController.php
 * 内容表单数据管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminFormDataController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        $formInfo=model('Form')->getInfo($formId);
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'form_id' => $formId,
        );
        $url = url('AdminFormData/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //表单数据列表信息
        $list = model('FormData')->loadData($formInfo['table'], $where, $formInfo['sequence'], $limit);
        $count = model('FormData')->countData($formInfo['table'], $where);
        //获取分页
        $page = $this->pageShow($count);
        //字段列表
        $fieldList=model('FormField')->loadData('form_id='.$formId);
        //模板赋值
        $this->assign('formInfo', $formInfo);
        $this->assign('fieldList', $fieldList);
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加表单数据
     */
    public function add()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        $formInfo=model('Form')->getInfo($formId);
        $html=model('FormData')->getField($formId);
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('html', $html);
        $this->assign('formInfo', $formInfo);
        $this->show('AdminFormData/info');
    }
    /**
     * 处理表单数据添加
     */
    public function addData()
    {
        if (empty($_POST['form_id'])) {
           $this->msg('无法获取表单ID！', false);
        }
        $checkField=model('FormData')->checkField($_POST);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }
        $_POST['data_id']=model('FormData')->addData($_POST);
        $this->msg('表单数据添加成功！', 1);
    }
    /**
     * 编辑表单数据资料
     */
    public function edit()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        $dataId = intval($_GET['data_id']);
        if (empty($dataId)) {
            $this->msg('无法获取数据ID！', false);
        }
        $formInfo=model('Form')->getInfo($formId);
        $html=model('FormData')->getField($formId,$dataId);
        //表单数据信息
        $info = model('FormData')->getInfo($formInfo['table'],$dataId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('html', $html);
        $this->assign('formInfo', $formInfo);
        $this->show('AdminFormData/info');
    }
    /**
     * 处理表单数据资料
     */
    public function editData()
    {
        if (empty($_POST['form_id'])) {
           $this->msg('无法获取表单ID！', false);
        }
        if (empty($_POST['data_id'])) {
            $this->msg('无法获取数据ID！', false);
        }
        $checkField=model('FormData')->checkField($_POST);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }
        model('FormData')->saveData($_POST);
        $this->msg('表单数据修改成功！', 1);
    }
    /**
     * 删除表单数据
     */
    public function del()
    {
        $data = trim($_POST['data']);
        if (empty($data)) {
            $this->msg('表单数据ID无法获取！', false);
        }
        $data=explode('-',$data);
        $formInfo=model('Form')->getInfo($data[1]);
        if(empty($formInfo)){
            $this->msg('表单ID无法获取！', false);
        }
        model('FormData')->delData($formInfo['table'],$data[0]);
        $this->msg('表单数据删除成功！');
    }
    
}