<?php
/**
 * RegionController.php
 * 地区列表
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class RegionController extends SiteController
{
    /**
     * 地区内容数据
     */
    public function json()
    {
        $parent_id = intval($_GET['parent_id']);
    	$list = model('Region')->loadData('parent_id = '.$parent_id);
        if(empty($list)){
            $this->msg($list,0);
        }else{
            $this->msg($list);
        }
    }
}