<?php
/**
 * AdminRewriteController.php
 * URL规则管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminRewriteController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //列表信息
        $list = model('Rewrite')->loadData($where, $limit);
        //模板赋值
        $this->assign('list', $list);
        $this->show();
    }
    /**
     * 保存配置
     */
    public function saveData()
    {
        $data=$_POST;
        if(empty($data['url'])){
            $this->msg('没有发现URL规则！', false);
        }
        $config='';
        foreach ($data['url'] as $key => $value) {
            if(!empty($value)&&!is_numeric($value)){
                $config.= "\$config['REWRITE']['".html_out($value)."'] = '".$data['model'][$key]."';\n";
            }
        }
        if(model('Rewrite')->saveData($config)){
            $this->msg('URL规则保存成功！', true);
        }else{
            $this->msg('URL规则保存失败，可能由于您的空间没有写入权限！', false);
        }
        
    }
}