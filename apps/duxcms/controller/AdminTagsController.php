<?php
/**
 * AdminTagsController.php
 * 内容分类
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminTagsController extends AdminController
{
    /**
     * 主页面
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //模型列表
        $modelList=model('ModelData')->loadData();
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword,
        );
        $url = url('AdminTags/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //菜单列表信息
        $list = model('Tags')->loadData($where, $limit);
        $count = model('Tags')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /*批量操作*/
    public function batch()
    {
        $tagId=implode(',',$_POST['id']);
        if(empty($tagId)){
            $this->alert('请先选择TAG！');
        }
        $type=intval($_POST['type']);
        if(empty($type)){
            $this->alert('请先选择操作！');
        }
        switch ($type) {
            case 1:
                //删除
                $data=array();
                model('Tags')->delData($tagId);
                model('TagsRelation')->delData($tagId);
                break;
        }
        $this->alert('批量操作成功！');
    }
}