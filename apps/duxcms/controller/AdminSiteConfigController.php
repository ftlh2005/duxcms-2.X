<?php
/**
 * AdminSiteConfigController.php
 * 当前站点设置
 * @author Life <349865361@qq.com>
 * @version 20140127
 */
class AdminSiteConfigController extends AdminController
{
    /**
     * 主页面
     */
    public function index()
    {
        $info = model('SiteConfig')->getInfo($this->siteConfig['site_id']);
        $this->assign('info', $info);
        $this->show();
    }
    /**
     * 编辑配置信息
     */
    public function editData()
    {
        $_POST['site_statistics'] = html_in($_POST['site_statistics']);
        $_POST['site_copyright'] = html_in($_POST['site_copyright']);
        $_POST['site_id'] = $this->siteConfig['site_id'];
        model('SiteConfig')->saveData($_POST);
        $this->msg('网站配置成功！', 1);
    }
}