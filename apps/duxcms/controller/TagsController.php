<?php
/**
 * TagsController.php
 * TAG列表
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class TagsController extends SiteController
{
    /**
     * TAG内容列表
     */
    public function index()
    {
    	$tag=urldecode($_GET['name']);
    	$tag = msubstr(in($tag),0,20);
    	if(empty($tag)){
    		$this->error404();
    		exit;
    	}
    	//查询TAG信息
    	$info = model('Tags')->getInfoName($tag);
    	if(empty($info)){
            $this->error404();
        }
        //更新点击计数
        $data=array();
        $data['click'] = $info['click'] + 1;
        $data['tag_id'] = $info['tag_id'];
        model('Tags')->saveData($data);
        //分页参数
        $listRows = $this->siteConfig['tpl_page_tags'];
        if(empty($listRows)){
            $listRows = 20;
        }
        $urlArray = array(
            'page' => '{page}',
            'name' => $tag,
        );
        $url = url('Tags/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'A.tag_id='.$info['tag_id'];
        //列表信息
        $list = model('TagsRelation')->loadData($where, $limit);
        $count = model('TagsRelation')->countData($where);
        if(!empty($list)){
            $appConfig=config('APP');
            $data=array();
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['curl']=api($value['app'],'getCurl',array('data'=>$value,'config'=>$appConfig));
                $data[$key]['aurl']=api($value['app'],'getAurl',array('data'=>$value,'config'=>$appConfig));
            }
        }
        //获取分页
        $page = $this->pageShow($count);
        //位置导航
        $crumb = array(
            0=>array('name'=>'TAG列表','url'=>url('Tags/tagList')),
            1=>array('name'=>$tag,'url'=> url('Tags/index',array('name'=>$tag))),
            );
        //MEDIA信息
        $media = model('Reception')->getMedia($this->siteConfig, 'tags - '.$tag);
        //模板赋值
        $this->assign('pageList', $data);
        $this->assign('crumb', $crumb);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->assign('tag', $tag);
        $this->show($this->siteConfig['tpl_name_tags']);
    }
    /**
     * TAG首页
     */
    public function tagList()
    {
        //分页参数
        $listRows = $this->siteConfig['tpl_page_tags'];
        if(empty($listRows)){
            $listRows = 20;
        }
        $urlArray = array(
            'page' => '{page}',
        );
        $url = url('Tags/tagList', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //菜单列表信息
        $list = model('Tags')->loadData($where, $limit);
        $count = model('Tags')->countData($where);
        if(!empty($list)){
            $data=array();
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['url']=url('Tags/index',array('name'=>$value['name']));
            }
        }
        //获取分页
        $page = $this->pageShow($count);
        //位置导航
        $crumb = array(
            0=>array('name'=>'TAG列表','url'=>url('Tags/tagList')),
            );
        //MEDIA信息
        $media = model('Reception')->getMedia($this->siteConfig, 'TAG列表');
        //模板赋值
        $this->assign('pageList', $data);
        $this->assign('crumb', $crumb);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        //模板名称处理
        $tplName=$this->siteConfig['tpl_name_tags'];
        $tplNameArray=explode('.', $tplName);
        $tplExt=end($tplNameArray);
        $tplName=substr($tplName, 0, -strlen($tplExt)-1);
        $this->show($tplName.'_index.'.$tplExt);
    }
}