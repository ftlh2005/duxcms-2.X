<?php
/**
 * IndexController.php
 * 默认页面
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class IndexController extends SiteController
{
    /**
     * 首页
     */
    public function index()
    {
    	//MEDIA信息
        $media=api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig));
    	$this->assign('media', $media);
        $this->show($this->siteConfig['tpl_name_index']);
    }
}