<?php
/**
 * CategoryController.php
 * 栏目页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class CategoryController extends SiteController
{
    /**
     * 栏目页
     */
    public function index()
    {
        $classId = intval($_GET['class_id']);
        $urlName = in($_GET['urlname']);
        if (empty($classId)&&empty($urlName)) {
            $this->error404();
        }
        //获取栏目信息
        if(!empty($classId)){
            $categoryInfo=model('CategoryData')->getInfo($classId);
        }else if(!empty($urlName)){
            $categoryInfo=model('CategoryData')->getInfoUrl($urlName);
        }else{
            $this->error404();
        }
        $classId = $categoryInfo['class_id'];
        //信息判断
        if (!is_array($categoryInfo)){
            $this->error404();
        }
        if($categoryInfo['app']<>APP_NAME){
            $this->error404();
        }
        //获取菜单信息
        $catalogInfo=api('duxcms','getCatalogClass',array('class_id'=>$classId));
        $categoryInfo['catalog_id']=$catalogInfo['catalog_id'];
        //位置导航
        $crumb=api('duxcms','loadCatalogCrumb',array('catalog_id'=>$catalogInfo['catalog_id']));
        //处理内容信息
        $pageInfo=model('CategoryPage')->getInfo($classId);
        $categoryInfo['content']=$pageInfo['content'];
        $categoryInfo['content']=api('duxcms','getReplaceContent',array('data'=>$categoryInfo['content']));
        //设置分页
        $appConfig=config('APP');
        $url = model('CategoryData')->getUrl($categoryInfo,$appConfig,true);
        $page = new Page();
        $content = $page->contentPage(html_out($categoryInfo['content']), "<hr class=\"ke-pagebreak\" />", $url, 10, 4); //文章分页
        $categoryInfo['content']=$content['content'];
        $page=$content['page']; 
        //查询上级栏目信息
        $parentCategoryInfo = model('CategoryData')->getInfo($categoryInfo['parent_id']);
        //获取顶级栏目信息
        $topCategoryInfo = model('CategoryData')->getInfo($crumb[0]['class_id']);
        //MEDIA信息
        $media=api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig,'title'=>$categoryInfo['name'],'keywords'=>$categoryInfo['keywords'],'description'=>$categoryInfo['description']));
        //模板赋值
        $this->assign('categoryInfo', $categoryInfo);
        $this->assign('parentCategoryInfo', $parentCategoryInfo);
        $this->assign('topCategoryInfo', $topCategoryInfo);
        $this->assign('crumb', $crumb);
        $this->assign('pageList', $pageList);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->show($categoryInfo['class_tpl']);
    }
}