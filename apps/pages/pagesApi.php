<?php
/**
 * pagesApi.php
 * 单页面管理
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class pagesApi extends BaseApi
{
	/**
     *菜单Api
     */
    public function apiAdminMenu()
    {
        return array(
            'content' => array(
                'menu' => array(
                    'pages' => array(
                    	'name' => '页面管理',
                        'order' => 3,
                        'menu' => array(
                            array(
                                'name' => '单页管理',
                                'url' => url('AdminPages/index'),
                                'ico' => '&#xf0f6;',
                                'order' => 1
                            )
                        ),
                    ),
                )
            )
        );
    }
    /**
     *模型Api
     */
    public function apiContentModel()
    {
        return array(
            'name'=>'页面',
            'listType'=>0,
            'order'=>0,
            );
    }
    /**
     *页面链接标签
     */
    public function apiLabelPageCurl($data){
        return model('CategoryData')->getCurl($data);
    }

}