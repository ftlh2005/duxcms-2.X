<?php
/**
 * SearchModel.php
 * 搜索处理
 * @author Life <349865361@qq.com>
 * @version 20140204
 */
class SearchModel extends BaseModel
{
    protected $table = 'content';
    /**
     * 文章列表
     * @param string $condition 查询条件
     * @param int $limit 条数
     * @param string $order 主排序
     * @param bool $extTable 内容表
     * @return array 用户信息
     */
    public function loadData($condition,$limit,$extTable=flase)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $field='A.*,B.name as cname,B.app,B.urlname as urlname';
        $obj=$this->model
                    ->field($field)
                    ->table('content', 'A')
                    ->join('category', 'B', array( 'A.class_id', 'B.class_id' ));
        //扩展字段表
        if(!empty($extTable)){
            $obj=$obj->field($field.',C.*')
                    ->leftJoin('content_article', 'C', array( 'A.content_id', 'C.content_id' ));
        }
        //文章列表
        $list=$obj->where($condition)
                    ->order('A.time DESC,A.content_id DESC')
                    ->limit($limit)
                    ->select();
        return $list;
    }
    /**
     * 文章总数
     * @param string $condition 查询条件
     * @param bool $extTable 内容表
     * @return int 数量
     */
    public function countData($condition,$extTable=flase)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $obj=$this->model
                    ->table('content', 'A')
                    ->join('category', 'B', array( 'A.class_id', 'B.class_id' ));
        //扩展字段表
        if(!empty($extTable)){
            $obj=$obj->leftJoin('content_article', 'C', array( 'A.content_id', 'C.content_id' ));
        }
        //文章统计
        $count=$obj->where($condition)
                    ->count();
        return $count;
    }
    
}