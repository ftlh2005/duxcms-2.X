<?php
/**
 * FieldsetModel.php
 * 文章表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class FieldsetModel extends BaseModel
{
    protected $table = 'fieldset';
    /**
     * 查询内容表信息
     * @param int $fieldsetId 字段集ID
     * @return array 信息
     */
    public function getInfo($fieldsetId)
    {
        return $this->find('fieldset_id='.$fieldsetId);
    }
    /**
     * 查询扩展表内容
     * @param int $fieldsetId 字段集ID
     * @return array 信息
     */
    public function getTableInfo($fieldsetId=null, $contentId=null)
    {
        if(empty($fieldsetId)||empty($contentId)){
            return array();
        }
        $fieldsetInfo=$this->getInfo($fieldsetId);
        return $this->model->table('fieldset_'.$fieldsetInfo['table'])->where('content_id='.$contentId)->find();
    }
}