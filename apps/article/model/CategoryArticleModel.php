<?php
/**
 * CategoryArticleModel.php
 * 文章栏目表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class CategoryArticleModel extends BaseModel
{
    protected $table = 'category_article';
    /**
     * 获取栏目列表
     * @param array $list 条件
     * @return array 栏目树列表
     */
    public function loadData($condition = null,$limit=null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $data=$this->model->field('A.*,B.*')
                        ->table('category','A')
                        ->leftJoin('category_article','B',array('A.class_id','B.class_id'))
                        ->order('A.sequence ASC,A.class_id ASC')
                        ->where($condition)
                        ->limit($limit)
                        ->select();
        return $data;
    }
    /**
     * 获取栏目分类数
     * @param array $list 条件
     * @return array 栏目树列表
     */
    public function getTrueData($condition = null, $classId=0)
    {
        $data=$this->loadData($condition);
        $cat = new Category(array('class_id', 'parent_id', 'name', 'cname'));
        $data=$cat->getTree($data, intval($classId));
        return $data;
    }
    /**
     * 获取栏目表总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model->table('category','A')
                        ->leftJoin('category_article','B',array('A.class_id','B.class_id'))
                        ->where($condition)
                        ->count();
    }
    /**
     * 栏目表信息
     * @param int $classId 栏目表ID
     * @return array 栏目信息
     */
    public function getInfo($classId)
    {
        return $this->find('class_id=' . $classId);
    }
    /**
     * 添加页面表信息
     * @param array $data 页面表信息
     * @return int 页面表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 保存页面表信息
     * @param array $data 页面表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->update('class_id=' . $data['class_id'], $data);
    }
    /**
     * 删除页面表信息
     * @param int $classId 页面表ID
     * @return bool 状态
     */
    public function delData($classId)
    {
        //删除关联内容
        return $this->delete('class_id=' . $classId);
    }
    /**
     * 数据格式化
     * @param array $data 页面信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $data['class_id']=intval($data['class_id']);
        //处理上传配置
        $data['config_upload']['THUMBNAIL_TYPE_INHERIT']=intval($data['config_upload']['THUMBNAIL_TYPE_INHERIT']);
        $data['config_upload']['THUMBNAIL_ON_INHERIT']=intval($data['config_upload']['THUMBNAIL_ON_INHERIT']);
        $data['config_upload']['THUMBNAIL_SIZE_INHERIT']=intval($data['config_upload']['THUMBNAIL_SIZE_INHERIT']);
        $data['config_upload']['WATERMARK_ON_INHERIT']=intval($data['config_upload']['WATERMARK_ON_INHERIT']);
        $data['config_upload']['WATERMARK_POSITION_INHERIT']=intval($data['config_upload']['WATERMARK_POSITION_INHERIT']);
        $data['config_upload']['WATERMARK_IMAGE_INHERIT']=intval($data['config_upload']['WATERMARK_IMAGE_INHERIT']);
        $data['config_upload']['UPLOAD_TYPE_INHERIT']=intval($data['config_upload']['UPLOAD_TYPE_INHERIT']);
        $data['config_upload']['UPLOAD_SIZE_INHERIT']=intval($data['config_upload']['UPLOAD_SIZE_INHERIT']);
        $data['config_upload']=serialize($data['config_upload']);
        return $data;
    }
    /**
     * 栏目列表标签
     * @param array $data 标签信息
     * @return array 菜单列表
     */
    public function loadLabelList($data)
    {
        $where='';
        if(isset($data['parentId'])){
            $where.=' AND A.parent_id='.$data['parentId'];
        }
        if(!empty($data['classId'])){
            $where.=' AND A.class_id in ('.$data['classId'].')';
        }
        if(isset($data['type'])){
            if($data['type']){
                $where.=' AND B.type = 1';
            }else{
                $where.=' AND B.type = 0';
            }
            
        }
        if(!empty($data['order'])){
            $where.=$data['where'];
        }
        if(!empty($data['where'])){
            $where.=$data['where'];
        }
        $list=$this->loadData('A.app="article"'.$where,$data['limit']);
        if(empty($list)){
            return array();
        }
        $appConfig=config('APP');
        $data=array();
        foreach ($list as $key => $value) {
            $data[$key]=$value;
            $data[$key]['curl']=model('CategoryData')->getUrl($value, $appConfig);
        }
        return $data;
    }
}