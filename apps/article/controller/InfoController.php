<?php
/**
 * InfoController.php
 * 栏目页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class InfoController extends SiteController
{
    /**
     * 内容页
     */
    public function index()
    {
        $contentId = intval($_GET['content_id']);
        $urlTitle = in($_GET['urltitle']);
        if (empty($contentId)&&empty($urlTitle)) {
            $this->error404();
        }
        //获取内容信息
        if(!empty($contentId)){
            $contentInfo=model('ContentData')->getInfo($contentId);
        }else if(!empty($urlTitle)){
            $contentInfo=model('ContentData')->getInfoUrl($urlTitle);
        }else{
            $this->error404();
        }
        $contentId = $contentInfo['content_id'];
        //信息判断
        if (!is_array($contentInfo)){
            $this->error404();
        }
        //获取扩展表
        $contentInfoExt = model('ContentArticle')->getInfo($contentId);
        $contentInfo = array_merge((array)$contentInfo,(array)$contentInfoExt);
        //获取栏目信息
        $categoryInfo=model('CategoryData')->getInfo($contentInfo['class_id']);
        if (!is_array($categoryInfo)){
            $this->error404();
        }
        if($categoryInfo['app']<>APP_NAME){
            $this->error404();
        };
        $categoryInfoExt = model('CategoryArticle')->getInfo($contentInfo['class_id']);
        $categoryInfo = array_merge((array)$categoryInfo,(array)$categoryInfoExt);
        //判断跳转
        if (!empty($contentInfo['url']))
        {
            $link=$this->display(html_out($contentInfo['url']),true,false);
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$link."");
            exit;
        }
        $contentInfo['urlname']=$categoryInfo['urlname'];
        $contentInfo['app']=$categoryInfo['app'];
        //获取扩展表内容
        $fieldsetData=model('Fieldset')->getTableInfo($categoryInfo['fieldset_id'],$contentInfo['content_id']);
        $contentInfo=array_merge((array)$fieldsetData,(array)$contentInfo);
        //获取菜单信息
        $catalogInfo=api('duxcms','getCatalogClass',array('class_id'=>$contentInfo['class_id']));
        $categoryInfo['catalog_id']=$catalogInfo['catalog_id'];
        //位置导航
        $crumb=api('duxcms','loadCatalogCrumb',array('catalog_id'=>$catalogInfo['catalog_id']));
        //查询上级栏目信息
        $parentCategoryInfo = model('CategoryData')->getInfo($categoryInfo['parent_id']);
        //获取顶级栏目信息
        $topCategoryInfo = model('CategoryData')->getInfo($crumb[0]['class_id']);
        //更新访问计数
        $viewsData=array();
        $viewsData['views']=$contentInfo['views']+1;
        $viewsData['content_id']=$contentInfo['content_id'];
        model('ContentData')->saveInfo($viewsData);
        //处理内容信息
        $contentInfo['content']=api('duxcms','getReplaceContent',array('data'=>$contentInfo['content']));
        if($contentInfo['taglink']){
            $contentInfo['content']=api('duxcms','getTagsContent',array('data'=>$contentInfo['content'],'content_id'=>$contentInfo['content_id']));
        }
        //设置内容分页
        $appConfig=config('APP');
        $url = model('ContentData')->getUrl($contentInfo,$appConfig,true);
        $page = new Page();
        $content = $page->contentPage(html_out($contentInfo['content']), "<hr style=\"page-break-after:always;\" class=\"ke-pagebreak\" />", $url, 10, 4); //文章分页
        $contentInfo['content']=$content['content'];
        $page=$content['page']; 
        //上下篇
        $prevInfo=model('ContentData')->getInfoWhere("A.time<{$contentInfo['time']} AND A.status=1 AND B.class_id={$categoryInfo['class_id']}",' A.time DESC,A.content_id DESC');
        if(!empty($prevInfo)){
            $prevInfo['aurl']=model('ContentData')->getUrl($prevInfo,$appConfig);
            $prevInfo['curl']=model('CategoryData')->getUrl($prevInfo,$appConfig);
        }
        //下一篇
        $nextInfo=model('ContentData')->getInfoWhere("A.time>{$contentInfo['time']} AND A.status=1 AND B.class_id={$categoryInfo['class_id']}",' A.time ASC,A.content_id ASC');
        if(!empty($nextInfo)){
            $nextInfo['aurl']=model('ContentData')->getUrl($nextInfo,$appConfig);
            $nextInfo['curl']=model('CategoryData')->getUrl($nextInfo,$appConfig);
        }
        //MEDIA信息
        $media=api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig,'title'=>$contentInfo['title'],'keywords'=>$contentInfo['keywords'],'description'=>$contentInfo['description']));
        //模板赋值
        $this->assign('contentInfo', $contentInfo);
        $this->assign('categoryInfo', $categoryInfo);
        $this->assign('parentCategoryInfo', $parentCategoryInfo);
        $this->assign('topCategoryInfo', $topCategoryInfo);
        $this->assign('crumb', $crumb);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->assign('prevInfo', $prevInfo);
        $this->assign('nextInfo', $nextInfo);
        if($contentInfo['tpl']){
            $this->show($contentInfo['tpl']);
        }else{
            $this->show($categoryInfo['content_tpl']);
        }
    }
}