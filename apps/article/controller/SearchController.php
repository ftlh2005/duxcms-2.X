<?php
/**
 * SearchController.php
 * 文章搜索页面
 * @author Life <349865361@qq.com>
 * @version 20140204
 */
class SearchController extends SiteController
{
    /**
     * 搜索结果
     */
    public function index() {
        $keyword=urldecode($_GET['keyword']);
        if(!is_utf8($keyword))
        {
            $keyword=auto_charset($keyword,'gbk','utf-8');
        }
        //解析关键词
        $keyword = msubstr(in($keyword),0,20);
        $keywords = preg_replace ('/\s+/',' ',$keyword); 
        $keywords=explode(" ",$keywords);
        if(empty($keywords[0])){
            $this->alert('没有输入关键词！');
        }
        $where='A.status=1 AND ';
        //获取栏目ID
        $classId = intval($_GET['class_id']);
        if($classId){
            $where.=' B.cid in('.$classId.') AND ';
        }
        //获取搜索类型
        $model=intval($_GET['model']);
        //分页参数
        $listRows = $this->siteConfig['tpl_page_search'];
        if(empty($listRows)){
            $listRows = 20;
        }
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $keyword,
            'class_id' => $classId,
            'model' => $model,
        );
        $url = url('Search/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //请求搜索结果
        $whereFor = '';
        foreach ($keywords as $value) {
            switch ($model) {
                //全文
                case '1':
                    $whereFor.= ' OR ( A.title like "%' . $value . '%" or A.keywords like "%' . $value . '%" or A.description like "%' . $value . '%" or C.content like "%' . $value . '%"  )';
                    $extTable = true;
                    break;
                //标题
                default:
                    $whereFor.= ' OR ( A.title like "%' . $value . '%" or A.keywords like "%' . $value . '%" or A.description like "%' . $value . '%" )';
                    break;
            }
        }
        $whereFor = substr($whereFor, 3);
        $where = $where.$whereFor;
        $list = model('Search')->loadData($where, $limit, $extTable);
        $count = model('Search')->countData($where, $extTable);
        if(!empty($list)){
            $appConfig=config('APP');
            $data=array();
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['curl']=api($value['app'],'getCurl',array('data'=>$value,'config'=>$appConfig));
                $data[$key]['aurl']=api($value['app'],'getAurl',array('data'=>$value,'config'=>$appConfig));
            }
        }
        //获取分页
        $page = $this->pageShow($count);
        //位置导航
        unset($urlArray['page']);
        $crumb = array(
            array('name'=>'搜索 - '.$keyword,'url'=> url('Search/index',$urlArray)),
            );
        //MEDIA信息
        $media=api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig,'title'=>'站内搜索 - '.$keyword));
        //模板赋值
        $this->assign('pageList', $data);
        $this->assign('crumb', $crumb);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->assign('keyword', $keyword);
        //模板名称处理
        $tplName=$this->siteConfig['tpl_name_search'];
        $tplNameArray=explode('.', $tplName);
        $tplExt=end($tplNameArray);
        $tplName=substr($tplName, 0, -strlen($tplExt)-1);
        $this->show($tplName.'_article.'.$tplExt);
    }
}