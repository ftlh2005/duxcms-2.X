<?php
/**
 * CategoryController.php
 * 栏目页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class CategoryController extends SiteController
{
    /**
     * 栏目页
     */
    public function index()
    {
        $classId = intval($_GET['class_id']);
        $urlName = in($_GET['urlname']);
        if (empty($classId)&&empty($urlName)) {
            $this->error404();
        }
        //获取栏目信息
        if(!empty($classId)){
            $categoryInfo=model('CategoryData')->getInfo($classId);
        }else if(!empty($urlName)){
            $categoryInfo=model('CategoryData')->getInfoUrl($urlName);
        }else{
            $this->error404();
        }
        $classId = $categoryInfo['class_id'];
        //信息判断
        if (!is_array($categoryInfo)){
            $this->error404();
        }
        if($categoryInfo['app']<>APP_NAME){
            $this->error404();
        }
        $categoryInfoExt = model('CategoryArticle')->getInfo($classId);
        $categoryInfo = array_merge((array)$categoryInfo,(array)$categoryInfoExt);
        //获取菜单信息
        $catalogInfo=api('duxcms','getCatalogClass',array('class_id'=>$classId));
        $categoryInfo['catalog_id']=$catalogInfo['catalog_id'];
        //位置导航
        $crumb=api('duxcms','loadCatalogCrumb',array('catalog_id'=>$catalogInfo['catalog_id']));
        //设置查询条件
        $where='';
        if ($categoryInfo['type'] == 0) {
            $classId = api('duxcms','getCatalogSubClass',array('catalog_id'=>$catalogInfo['catalog_id']));
        }
        if(empty($classId)){
            $classId = $categoryInfo['class_id'];
        }
        $where .= 'A.status=1 AND B.class_id in (' . $classId . ')';
        //分页参数
        $size = intval($categoryInfo['page']); 
        if (empty($size)) {
            $listRows = 20;
        } else {
            $listRows = $size;
        }
        //设置URL
        $appConfig=config('APP');
        $url = model('CategoryData')->getUrl($categoryInfo,$appConfig,true);
        $limit=$this->pageLimit($url,$listRows);
        //获取字段集信息
        $fieldsetInfo=model('Fieldset')->getInfo($categoryInfo['fieldset_id']);
        //内容列表信息
        $pageList = model('ContentData')->loadData($where,$limit,'A.type_top > '.time().' DESC,'.$categoryInfo['content_order'],$fieldsetInfo['table']);
        $count = model('ContentData')->countData($where);
        $list=array();
        if(!empty($pageList)){
            foreach ($pageList as $key=>$value) {
                $list[$key]=$value;
                $list[$key]['curl']=model('CategoryData')->getUrl($value, $appConfig);
                $list[$key]['aurl']=model('ContentData')->getUrl($value, $appConfig);
            }
        }
        $pageList=$list;
        //获取分页
        $page = $this->pageShow($count);
        //查询上级栏目信息
        $parentCategoryInfo = model('CategoryData')->getInfo($categoryInfo['parent_id']);
        //获取顶级栏目信息
        $topCategoryInfo = model('CategoryData')->getInfo($crumb[0]['class_id']);
        //MEDIA信息
        $media=api('duxcms','getReceptionMedia',array('config'=>$this->siteConfig,'title'=>$categoryInfo['name'],'keywords'=>$categoryInfo['keywords'],'description'=>$categoryInfo['description']));
        //模板赋值
        $this->assign('categoryInfo', $categoryInfo);
        $this->assign('parentCategoryInfo', $parentCategoryInfo);
        $this->assign('topCategoryInfo', $topCategoryInfo);
        $this->assign('crumb', $crumb);
        $this->assign('pageList', $pageList);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('media', $media);
        $this->show($categoryInfo['class_tpl']);
    }
}