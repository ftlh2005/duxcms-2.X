<?php
/**
 * AdminCategoryController.php
 * 内容模型操作
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminCategoryController extends AdminController
{
    /**
     * 主页面
     */
    public function index()
    {
        //栏目列表信息
        $list = model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0);
        $count = model('CategoryArticle')->countData('A.app="'.APP_NAME.'"');
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['curl']=model('CategoryData')->getUrl($value, $appConfig);
            }
        }
        $list = $data;
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->show();
    }
    /**
     * 添加栏目
     */
    public function add()
    {
        $appConfig = appConfig('article');
        $info = $appConfig['APP_MODEL_CONFIG'];
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('categoryList', model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0));
        $this->assign('FieldsetList', api('duxcms','loadFieldsetData'));
        $this->show('AdminCategory/info');
    }
    /**
     * 处理栏目添加
     */
    public function addData()
    {
        $_POST['app']=APP_NAME;
        $list = explode("\n", $_POST['namelist']);
        if(empty($list)){
            $this->msg('栏目名称未填写',false);
        }
        if(empty($_POST['class_tpl'])&&empty($_POST['content_tpl'])){
            $this->msg('栏目或内容模板未设置！',false);
        }
        foreach ($list as $name) {
            if(empty($name)){
                continue;
            }
            $nameArray=explode('|',$name);
            if(empty($nameArray)){
                continue;
            }
            $_POST['name']=$nameArray[0];
            $_POST['urlname']=$nameArray[1];
            $classId=api('duxcms', 'addCategoryData',$_POST);
            $data=$_POST;
            $data['class_id']=$classId;
            model('CategoryArticle')->addData($data);
        }
        //关联附件
        api('admin','relationFile',array('relation_id'=>$classId,'model'=>'category_atricle','relation_key'=>$_POST['relation_key']));
        $this->msg('栏目添加成功！');
    }
    /**
     * 编辑栏目
     */
    public function edit()
    {
        $classId = intval($_GET['class_id']);
        if (empty($classId)) {
            $this->msg('无法获取栏目ID！', false);
        }
        //栏目信息
        $info = api('duxcms', 'getCategoryData',array('class_id'=>$classId));
        $tableInfo = model('CategoryArticle')->getInfo($classId);
        $info = array_merge((array)$info,(array)$tableInfo);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('configUpload', unserialize($info['config_upload']));
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('categoryList', model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0));
        $this->assign('FieldsetList', api('duxcms','loadFieldsetData'));
        $this->show('AdminCategory/info');
    }
    /**
     * 处理栏目
     */
    public function editData()
    {
        $data=in($_POST);
        if (empty($data['class_id'])) {
            $this->msg('无法获取栏目ID！', false);
        }
        //检查
        if(empty($data['name'])){
            $this->msg('栏目名称未填写',false);
        }
        if(empty($data['class_tpl'])&&empty($data['content_tpl'])){
            $this->msg('栏目或内容模板未选择！',false);
        }
        // 分类检测
        if ($data['parent_id'] == $data['class_id']){
            $this->msg('不可以将当前栏目设置为上一级栏目！',false);
            return;
        }
        $cat = api('duxcms','getCategoryTree',array('class_id'=>$data['class_id']));
        if (!empty($cat)) {
            foreach ($cat as $vo) {
                if ($data['parent_id'] == $vo['class_id']) {
                    $this->msg('不可以将上一级栏目移动到子栏目！',false);
                    return;
                }
            }
        }
        api('duxcms', 'saveCategoryData',$_POST);
        model('CategoryArticle')->saveData($_POST);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$data['class_id'],'model'=>'category_atricle','relation_key'=>$_POST['relation_key']));
        $this->msg('栏目修改成功！');
    }
    /**
     * 删除栏目
     */
    public function del()
    {
        $classId=intval($_GET['class_id']);
        if (empty($classId)) {
            $this->msg('无法获取栏目ID！', false);
        }
        if(api('duxcms', 'countCategoryData',array('where'=>'parent_id='.$classId))){
            $this->msg('请先删除子栏目！',false);
        }
        if(api('duxcms','countContentData',array('where'=>'A.class_id='.$classId))){
            $this->msg('请先删除栏目下内容！',false);
        }
        //删除栏目
        api('duxcms', 'delCategoryData',array('class_id'=>$classId));
        model('CategoryArticle')->delData($classId);
        //关联附件
        api('admin','delRelationFile',array('relation_id'=>$classId,'model'=>'category_atricle'));
        $this->msg('栏目删除成功！',1);
    }
}