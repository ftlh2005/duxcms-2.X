<?php 
return array (
  'APP_STATE' => 1,
  'APP_NAME' => '新闻发布',
  'APP_VER' => '20140111',
  'APP_AUTHOR' => 'Life',
  'APP_ORIGINAL_PREFIX' => 'dux_',
  'APP_TABLES' => '',
  'APP_DESCRIPTION' => '新闻发布模型',
  'APP_SYSTEM' => 1,
  'APP_INSTALL' => 1,
  'APP_MODEL_CONFIG' => 
  array (
    'page' => '10',
    'content_order' => 'time DESC',
    'class_tpl' => 'list.html',
    'content_tpl' => 'content.html',
    'fieldset_id' => '0',
    'copyfrom' => 'duxcms',
  ),
);