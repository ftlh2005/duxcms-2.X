<?php
/**
 * UserLogController.php
 * 用户管理页面
 * @author Life <349865361@qq.com>
 * @version 20140112
 */
class UserLogController extends AdminController
{
    /**
     * 列表
     */
    public function index()
    {
        //筛选条件
        $filterType = intval($_GET['type']);
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterType)) {
            $filterWhere .= ' AND A.type=' . $filterType;
        }
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND B.username LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'type' => $filterType,
            'keyword' => $filterKeyword
        );
        $url = url('UserLog/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'B.level<=' . intval($this->userInfo['level']) . ' AND C.level<=' . intval($this->userInfo['group_level']) . $filterWhere;
        //用户列表信息
        $list = model('UserLog')->loadData($where, $limit);
        $count = model('UserLog')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('type', model('UserLog')->getType());
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
}