<?php
/**
 * SettingController.php
 * 系统设置
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class SettingController extends AdminController
{
    /**
     * 后台设置
     */
    public function admin()
    {
        $this->show();
    }
    /**
     * 性能设置
     */
    public function performance()
    {
        $this->show();
    }
    /**
     * 上传设置
     */
    public function upload()
    {
        $this->show();
    }
    /**
     * 保存设置
     */
    public function saveData()
    {
        $config = in($_POST);
        model('Setting')->setConfig($config);
        $this->msg('网站配置成功！');
    }
}