<?php
/**
 * AppManageController.php
 * 系统功能管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AppManageController extends AdminController
{
    /**
     * 管理列表
     */
    public function index()
    {
        $this->AppList = model('AppData')->loadList();
        $this->show();
    }
    /**
     * 安装
     * @param string $_POST['name'] APP标识
     */
    public function install()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if ($info['APP_INSTALL'] == 1) {
            $this->msg('该APP已经安装过了！', false);
        }
        if ($info['APP_RELY']) {
            $appRely = explode(',', $info['APP_RELY']);
            if (!empty($appRely)) {
                foreach ($appRely as $value) {
                    $appConfig = appConfig($value);
                    if (empty($appConfig)) {
                        $this->msg('没有发现依赖APP {' . $value . '}', false);
                    }
                    if (!$appConfig['APP_STATE']) {
                        $this->msg('请先开启{' . $appConfig['APP_NAME'] . '}后才能安装！', false);
                    }
                }
            }
        }
        $appBase = ROOT_PATH . 'apps/' . $name . '/data/';
        //导入数据库
        $dbDir = $appBase . 'install_data/';
        $dbConfig = config('DB');
        if (is_dir($dbDir)) {
            model('AppData')->installData($dbDir, $dbConfig, $info['APP_ORIGINAL_PREFIX']);
        }
        //写入配置
        $data = array();
        $data['APP_STATE'] = 1;
        $data['APP_INSTALL'] = 1;
        if (save_config($name, $data)) {
            model('Cache')->clearCache('api');
            model('Cache')->clearCache('menu');
            api($name, 'ApiAppInstall');
            $this->msg('安装成功！');
        }
        $this->msg('安装失败！可能由于您的目录没有读写权限！', false);
    }
    /**
     * 卸载
     * @param string $_POST['name'] APP标识
     */
    public function uninstall()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if (!$info['APP_INSTALL']) {
            $this->msg('该APP已经卸载或者未安装！', false);
        }
        if ($info['APP_STATE']) {
            $this->msg('请先禁用本APP才能进行卸载！', false);
        }
        //卸载数据库
        if (!empty($info['APP_TABLES'])) {
            model('AppData')->uninstallData($info['APP_TABLES']);
        }
        api($name, 'ApiAppUninstall');
        //写入配置
        $data = array();
        $data['APP_STATE'] = 0;
        $data['APP_INSTALL'] = 0;
        if (save_config($name, $data)) {
            model('Cache')->clearCache('api');
            model('Cache')->clearCache('menu');
            $this->msg('卸载成功，请手动FTP删除文件！');
        }
    }
    /**
     * 备份
     * @param string $_POST['name'] APP标识
     */
    public function backup()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if (!$info['APP_INSTALL']) {
            $this->msg('该APP已经卸载或者未安装！', false);
        }
        //导出数据库
        $appBase = ROOT_PATH . 'apps/' . $name . '/data/';
        $dbDir = $appBase . 'backup_data/';
        if (empty($info['APP_TABLES'])) {
            $this->msg('该APP无需备份数据！', false);
        }
        $dbConfig = config('DB');
        $status = model('AppData')->backupData($dbDir, $dbConfig, $info['APP_TABLES']);
        if (!$status) {
            $this->msg('数据备份失败！', false);
        }
        $this->msg('备份当前数据成功！');
    }
    /**
     * 打包
     * @param string $_POST['name'] APP标识
     */
    public function package()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if (!$info['APP_INSTALL']) {
            $this->msg('该APP已经卸载或者未安装！', false);
        }
        //导出数据库
        $appBase = ROOT_PATH . 'apps/' . $name . '/data/';
        $dbDir = $appBase . 'install_data/';
        if (empty($info['APP_TABLES'])) {
            $this->msg('该APP无需打包数据！', false);
        }
        $dbConfig = config('DB');
        $status = model('AppData')->backupData($dbDir, $dbConfig, $info['APP_TABLES']);
        if (!$status) {
            $this->msg('打包备份失败！', false);
        }
        //写入配置
        $data = array();
        $data['APP_ORIGINAL_PREFIX'] = $dbConfig['DB_PREFIX'];
        if (save_config($name, $data)) {
            $this->msg('打包当前数据成功！');
        }
        $this->msg('打包失败！可能由于您的目录没有读写权限！', false);
    }
    /**
     * 恢复
     * @param string $_POST['name'] APP标识
     */
    public function restore()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if (!$info['APP_INSTALL']) {
            $this->msg('该APP已经卸载或者未安装！', false);
        }
        $appBase = ROOT_PATH . 'apps/' . $name . '/data/';
        $dbDir = $appBase . 'backup_data/';
        if (is_dir($dbDir)) {
            $dbConfig = config('DB');
            model('AppData')->installData($dbDir, $dbConfig, $info['APP_ORIGINAL_PREFIX']);
        } else {
            $this->msg('没有发现备份过的数据！', false);
        }
        $this->msg('当前APP数据恢复成功！');
    }
    /**
     * 禁用启用
     * @param string $_POST['name'] APP标识
     */
    public function status()
    {
        $name = in($_POST['name']);
        if (empty($name)) {
            $this->msg('无法获取APP标识！', false);
        }
        //获取APP信息
        $info = appConfig($name);
        if (empty($info)) {
            $this->msg('无法获取APP配置！', false);
        }
        //设置状态
        if (!$info['APP_STATE']) {
            //获取当前APP依赖
            $appRely = $info['APP_RELY'];
            if (!empty($appRely)) {
                $appRelyList = explode(',', $appRely);
                if (!empty($appRelyList)) {
                    foreach ($appRelyList as $value) {
                        $appConfig = appConfig($value);
                        if (!$appConfig['APP_STATE']) {
                            $this->msg('请先开启{' . $appConfig['APP_NAME'] . '}后才能操作！', false);
                        }
                    }
                }
            }
            $status = 1;
        } else {
            //检测是否存在下级依赖
            $appList = getApps();
            foreach ($appList as $value) {
                $appConfig = appConfig($value);
                $appRely = $appConfig['APP_RELY'];
                if (empty($appRely)) {
                    continue;
                }
                $appRelyList = explode(',', $appRely);
                if (in_array($name, $appRelyList)) {
                    if ($appConfig['APP_STATE']) {
                        $this->msg('请先关闭{' . $appConfig['APP_NAME'] . '}后才能操作！', false);
                    }
                }
            }
            $status = 0;
        }
        //写入配置
        $data = array();
        $data['APP_STATE'] = $status;
        if (save_config($name, $data)) {
            model('Cache')->clearCache('api');
            model('Cache')->clearCache('menu');
            $this->msg('状态更改成功！');
        }
        $this->msg('状态更改失败！可能由于您的配置文件没有权限！', false);
    }
}