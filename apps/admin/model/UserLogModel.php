<?php
/**
 * UserLogModel.php
 * 用户操作记录
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class UserLogModel extends BaseModel
{
    protected $table = 'user_log';
    /**
     * 操作类型
     * @param int $type 状态
     * @return array 详细
     */
    public function getType($type = null)
    {
        $data = array(
            1 => '登录',
            2 => '添加',
            3 => '编辑',
            4 => '删除'
        );
        if ($type) {
            return $data[$type];
        } else {
            return $data;
        }
    }
    /**
     * 获取用户组列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return bool 状态
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->model->field('A.*,B.username,C.level')->table('user_log', 'A')->leftJoin('user', 'B', array(
            'A.user_id',
            'B.user_id'
        ))->leftJoin('user_group', 'C', array(
            'C.group_id',
            'B.group_id'
        ))->where($condition)->order('A.log_id DESC')->limit($limit)->select();
    }
    /**
     * 获取用户组总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->model->table('user_log', 'A')->leftJoin('user', 'B', array(
            'A.user_id',
            'B.user_id'
        ))->leftJoin('user_group', 'C', array(
            'C.group_id',
            'B.group_id'
        ))->where($condition)->count();
    }
    /**
     * 增加登录信息
     * @param array $data 用户信息
     * @return int 登录记录ID
     */
    public function addData($data)
    {
        $logNum = $this->count();
        if ($loginnum > 5000) {
            $this->model->table('user_log')->order('id asc')->limit(1)->delete();
        }
        $logData = array();
        $logData['user_id'] = $data['user_id'];
        $logData['time'] = time();
        $logData['ip'] = get_client_ip();
        $logData['type'] = $data['type'];
        $logData['content'] = $data['content'];
        return $this->model->table('user_log')->data($logData)->insert();
    }
}