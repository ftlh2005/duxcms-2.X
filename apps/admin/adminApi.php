<?php
/**
 * adminApi.php
 * 管理系统API
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class adminApi extends BaseApi
{
    /**
     * 菜单Api
     * 多维数组，第一维主菜单，第二维归类菜单，第三维菜单列表
     * @param string key 菜单标识
     * @param string name 功能描述
     * @param array menu 子菜单数组
     */
    public function apiAdminMenu()
    {
        return array(
            'index' => array(
                'name' => '首页',
                'order' => 0,
                'menu' => array(
                    'default' => array(
                        'name' => '快捷菜单',
                        'menu' => array(
                            array(
                                'name' => '管理首页',
                                'url' => url('Index/home'),
                                'ico' => '&#xf05a;',
                                'order' => 0
                            )
                        )
                    )
                )
            ),
            'system' => array(
                'name' => '系统',
                'order' => 9,
                'menu' => array(
                    'setting' => array(
                        'name' => '系统设置',
                        'order' => 0,
                        'menu' => array(
                            array(
                                'name' => '后台设置',
                                'url' => url('Setting/admin'),
                                'ico' => '&#xf0a3;',
                                'order' => 1
                            ),
                            array(
                                'name' => '性能设置',
                                'url' => url('Setting/performance'),
                                'ico' => '&#xf0e4;',
                                'order' => 1
                            ),
                            array(
                                'name' => '上传设置',
                                'url' => url('Setting/upload'),
                                'ico' => '&#xf08b;',
                                'order' => 2
                            )
                        )
                    ),
                    'manage' => array(
                        'name' => '系统管理',
                        'order' => 1,
                        'menu' => array(
                            array(
                                'name' => '缓存管理',
                                'url' => url('Manage/cache'),
                                'ico' => '&#xf0a0;',
                                'order' => 0
                            ),
                            array(
                                'name' => 'APP管理',
                                'url' => url('AppManage/index'),
                                'ico' => '&#xf06b;',
                                'order' => 1
                            ),
                            array(
                                'name' => '安全记录',
                                'url' => url('UserLog/index'),
                                'ico' => '&#xf070;',
                                'order' => 2
                            )
                        )
                    ),
                    'user' => array(
                        'name' => '用户管理',
                        'order' => 2,
                        'menu' => array(
                            array(
                                'name' => '用户管理',
                                'url' => url('User/index'),
                                'ico' => '&#xf007;',
                                'order' => 0
                            ),
                            array(
                                'name' => '用户组管理',
                                'url' => url('UserGroup/index'),
                                'ico' => '&#xf0c0;',
                                'order' => 1
                            )
                        )
                    )
                )
            )
        );
    }
    /**
     * 权限Api
     * 当前APP权限控制，第一维控制器名称，第二维方法
     * @param string name 控制器描述
     * @param array auth 对象方法=>描述
     */
    public function apiAdminPurview()
    {
        return array(
            'Setting' => array(
                'name' => '系统设置',
                'auth' => array(
                    'admin' => '后台设置',
                    'performance' => '性能设置',
                    'upload' => '上传设置'
                )
            ),
            'Manage' => array(
                'name' => '系统管理',
                'auth' => array(
                    'cache' => '缓存管理'
                )
            ),
            'AppManage' => array(
                'name' => 'APP管理',
                'auth' => array(
                    'index' => '浏览'
                )
            )
        );
    }
    /**
     * 获取指定菜单列表
     * @param string $data['key'] 主菜单KEY
     */
    public function getAdminMenu($data)
    {
        return model('Menu')->getMenuList($data['key']);
    }
    /**
     * 获取用户组信息
     * @param array $data 用户组信息
     */
    public function getGroupInfo($data)
    {
        return model('UserGroup')->getInfo($data['group_id']);
    }
    /**
     * 保存配置文件
     * @param array $data 配置数组
     */
    public function setConfig($data)
    {
        return model('Setting')->setConfig($data);
    }
    /**
     * 上传接口
     * @param array $data['files'] 文件数组__FILES__
     * @param array $data 上传配置文件
     * @return array 上传后文件信息
     */
    public function addUpload($data)
    {
        return model('Upload')->uploadData($data['files'],$data['config']);
    }
    /**
     * 用户记录
     * @param array $data 安全信息
     * @return int 记录ID
     */
    public function setUserLog($data)
    {
        return model('UserLog')->addData($data);
    }
    /**
     * 关联附件文件
     * @param array $data['relation_id'] 要关联的ID
     * @param array $data['model'] 需要关联的APP或标识
     * @param array $data['relation_key'] 关联KEY一般为表单生成值
     * @return bool 成功信息
     */
    public function relationFile($data)
    {
        return model('Attachment')->relationData($data['relation_id'],$data['model'],$data['relation_key']);
    }
    /**
     * 删除关联附件文件
     * @param array $data['relation_id'] 要关联的ID
     * @param array $data['model'] 需要关联的APP或标识
     * @return bool 成功信息
     */
    public function delRelationFile($data)
    {
        return model('Attachment')->delRelationData($data['relation_id'],$data['model']);
    }
    /**
     * 获取附件文件
     * @param array $data['file_id'] 附件ID
     * @return bool 成功信息
     */
    public function getFileList($data)
    {
        return model('Attachment')->getFileList($data['file_id']);
    }
    /**
     *文章链接标签
     */
    public function apiLabelFileList($data){
        return model('Attachment')->getLabelList($data);
    }
    /**
     * 刷新缓存
     * @param int $data['type'] 缓存类型
     * @return bool 成功信息
     */
    public function resetCache($data)
    {
        return model('Cache')->clearCache($data['type']);
    }


}