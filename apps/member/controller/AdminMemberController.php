<?php
/**
 * AdminMemberController.php
 * 会员管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminMemberController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterGroupId = intval($_GET['group_id']);
        $filterStatus = intval($_GET['status']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND A.username LIKE "%' . $filterKeyword . '%"';
        }
        if (!empty($filterGroupId)) {
            $filterWhere .= ' AND A.group_id = ' . $filterGroupId;
        }
        if (!empty($filterStatus)) {
            $filterWhere .= ' AND A.status = ' . $filterStatus;
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword,
            'group_id' => $filterGroupId,
            'status' => $filterStatus,
        );
        $url = url('AdminMember/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = substr($filterWhere, 4);
        //会员列表信息
        $list = model('Member')->loadData($where, $limit);
        $count = model('Member')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //会员组
        $groupList = model('MemberGroup')->getTrueData();
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('groupList', $groupList);
        $this->show();
    }
    /**
     * 添加会员
     */
    public function add()
    {
        $expand=model('MemberField')->getField();
        $groupList = model('MemberGroup')->getTrueData(); 
        $roleList = model('MemberRole')->loadData(); 
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('expand', $expand);
        $this->assign('groupList', $groupList);
        $this->assign('roleList', $roleList);
        $this->show('adminmember/info');
    }
    /**
     * 处理会员添加
     */
    public function addData()
    {
        $data = in($_POST);
        if (empty($data['username'])) {
            $this->msg('会员用户名未填写！', false);
        }
        if(!Check::email($data['email'])||empty($data['email'])){
            $this->msg('邮箱地址输入不正确',0);
        }
        if (empty($data['password'])||empty($data['password2'])) {
            $this->msg('密码或确认密码未填写！', false);
        }
        if($data['password']<>$data['password2']){
            $this->msg('两次密码输入不同！', false);
        }
        $data['password'] = md5($data['password']);
        if(model('Member')->getRepeat('username',$data['username'])){
            $this->msg('该用户名已被注册，请更换其他用户名！',0);
        }
        if(model('Member')->getRepeat('email',$data['email'])){
            $this->msg('该邮箱已被注册，请更换其他邮箱！',0);
        }
        $checkField=model('MemberField')->checkField($data);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }
        $data['user_id'] = model('Member')->addData($data);
        model('MemberData')->saveData($data);
        $this->msg('会员添加成功！');
    }
    /**
     * 编辑会员资料
     */
    public function edit()
    {
        $userId = intval($_GET['user_id']);
        if (empty($userId)) {
            $this->msg('无法获取会员ID！', false);
        }
        //会员信息
        $info = model('Member')->getInfo($userId);
        $expand=model('MemberField')->getField($userId);
        $groupList = model('MemberGroup')->getTrueData();
        $roleList = model('MemberRole')->loadData(); 
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('expand', $expand);
        $this->assign('groupList', $groupList);
        $this->assign('roleList', $roleList);
        $this->show('adminmember/info');
    }
    /**
     * 处理会员资料
     */
    public function editData()
    {
        $data = in($_POST);
        $userId = intval($data['user_id']);
        if (empty($userId)) {
            $this->msg('无法获取会员ID！', false);
        }
        if (empty($data['username'])) {
            $this->msg('会员用户名未填写！', false);
        }
        if(!Check::email($data['email'])||empty($data['email'])){
            $this->msg('邮箱地址输入不正确',0);
        }
        if(!empty($data['password'])){
            if (empty($data['password'])||empty($data['password2'])) {
                $this->msg('密码或确认密码未填写！', false);
            }
            if($data['password']<>$data['password2']){
                $this->msg('两次密码输入不同！', false);
            }
            $data['password'] = md5($data['password']);
        }else{
            unset($data['password']);
        }
        
        if(model('Member')->getRepeat('username',$data['username'], $userId)){
            $this->msg('该用户名已被注册，请更换其他用户名！',0);
        }
        if(model('Member')->getRepeat('email',$data['email'], $userId)){
            $this->msg('该邮箱已被注册，请更换其他邮箱！',0);
        }

        $checkField=model('MemberField')->checkField($data);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }

        model('Member')->saveData($data);
        model('MemberData')->saveData($data);
        $this->msg('会员修改成功！');
    }
    /**
     * 删除会员
     */
    public function del()
    {
        $userId = intval($_POST['data']);
        if (empty($userId)) {
            $this->msg('会员ID无法获取！', false);
        }
        model('Member')->delData($userId);
        $data['user_id'] = $userId;
        $this->msg('会员删除成功！');
    }
}