<?php
/**
 * AdminMemberGroupController.php
 * 会员组管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminMemberGroupController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminMemberGroup/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //会员组列表信息
        $list = model('MemberGroup')->getTrueData($where, $limit);
        $count = model('MemberGroup')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //获取编辑接口
        $funList = hook_api('apiMemberGroupFun');
        $data=array();
        if(!empty($funList)){
            foreach ($funList as $app => $value) {
                if(!empty($value)&&is_array($value)){
                    foreach ($value as $key => $vo) {
                        $data[]=$vo;
                    }
                }
            }
        }
        $funList = array_order($data, 'order', 'asc');
        //模板赋值
        $this->assign('funList', $funList);
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加会员组
     */
    public function add()
    {
        //模板赋值
        $this->assign('groupList', model('MemberGroup')->getTrueData());
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminmembergroup/info');

    }
    /**
     * 处理会员组添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('会员组名称未填写！', false);
        }
        $_POST['base_purview'] = serialize($_POST['MemberPurvew']);
        $_POST['menu_purview'] = serialize($_POST['MemberMenu']);
        model('MemberGroup')->addData($_POST);
        $this->msg('会员组添加成功！', 1);
    }
    /**
     * 编辑会员组资料
     */
    public function edit()
    {
        $groupId = intval($_GET['group_id']);
        if (empty($groupId)) {
            $this->msg('无法获取会员组ID！', false);
        }
        //会员组信息
        $info = model('MemberGroup')->getInfo('group_id',$groupId);
        //模板赋值
        $this->assign('groupList', model('MemberGroup')->getTrueData());
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminmembergroup/info');
    }
    /**
     * 处理会员组资料
     */
    public function editData()
    {
        $data = in($_POST);
        $groupId = intval($_POST['group_id']);
        if (empty($groupId)) {
            $this->msg('无法获取会员组ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('会员组名称未填写！', false);
        }
        if ($data['parent_id'] == $data['group_id']){
            $this->msg('不可以将当前用户组设置为上一级用户组！',false);
        }
        $cat = model('MemberGroup')->getTrueData('',$data['group_id']);
        if (!empty($cat)) {
            foreach ($cat as $vo) {
                if ($data['parent_id'] == $vo['catalog_id']) {
                    $this->msg('不可以将上一级用户组移动到子用户组！',false);
                }
            }
        }
        model('MemberGroup')->saveData($_POST);
        $this->msg('会员组修改成功！', 1);
    }
    /**
     * 编辑会员组权限
     */
    public function purview()
    {
        $groupId = intval($_GET['group_id']);
        if (empty($groupId)) {
            $this->msg('无法获取会员组ID！', false);
        }
        //会员组信息
        $info = model('MemberGroup')->getInfo('group_id',$groupId);
        $MemberPurvewArray = unserialize($info['base_purview']);
        $MemberMenuArray = unserialize($info['menu_purview']);
        //模板赋值
        $this->assign('MenberPurvew', model('MemberGroup')->getMemberPurview());
        $this->assign('MemberMenu', model('MemberMenu')->getMenuList(false));
        $this->assign('MemberPurvewArray', $MemberPurvewArray);
        $this->assign('MemberMenuArray', $MemberMenuArray);
        $this->assign('groupList', model('MemberGroup')->getTrueData());
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminmembergroup/purview');
    }
    /**
     * 处理会员组权限
     */
    public function purviewData()
    {
        $data = in($_POST);
        $groupId = intval($_POST['group_id']);
        if (empty($groupId)) {
            $this->msg('无法获取会员组ID！', false);
        }
        $_POST['base_purview'] = serialize($_POST['MemberPurvew']);
        $_POST['menu_purview'] = serialize($_POST['MemberMenu']);
        model('MemberGroup')->saveData($_POST);
        $this->msg('会员组修改成功！', 1);
    }
    /**
     * 删除会员组
     */
    public function del()
    {
        $groupId = intval($_POST['data']);
        if (empty($groupId)) {
            $this->msg('会员组ID无法获取！', false);
        }
        model('MemberGroup')->delData($groupId);
        $this->msg('会员组删除成功！');
    }
}