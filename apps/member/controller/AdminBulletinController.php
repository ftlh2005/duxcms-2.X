<?php
/**
 * AdminBulletinController.php
 * 会员公告管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminBulletinController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' title LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminBulletin/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //会员公告列表信息
        $list = model('MemberBulletin')->loadData($where, $limit);
        $count = model('MemberBulletin')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加会员公告
     */
    public function add()
    {
        $groupList = model('MemberGroup')->loadData();
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '发布');
        $this->assign('groupList', $groupList);
        $this->show('adminbulletin/info');
    }
    /**
     * 处理会员公告添加
     */
    public function addData()
    {
        if (empty($_POST['title'])) {
            $this->msg('公告标题未填写！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('公告内容未填写！', false);
        }
        model('MemberBulletin')->addData($_POST);
        $this->msg('会员公告添加成功！', 1);
    }
    /**
     * 编辑会员公告资料
     */
    public function edit()
    {
        $bullId = intval($_GET['bull_id']);
        if (empty($bullId)) {
            $this->msg('无法获取会员公告ID！', false);
        }
        $groupList = model('MemberGroup')->loadData();
        //会员公告信息
        $info = model('MemberBulletin')->getInfo('bull_id',$bullId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('groupList', $groupList);
        $this->show('adminbulletin/info');
    }
    /**
     * 处理会员公告资料
     */
    public function editData()
    {
        $bullId = intval($_POST['bull_id']);
        if (empty($bullId)) {
            $this->msg('无法获取会员公告ID！', false);
        }
        if (empty($_POST['title'])) {
            $this->msg('公告标题未填写！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('公告内容未填写！', false);
        }
        model('MemberBulletin')->saveData($_POST);
        $this->msg('会员公告修改成功！', 1);
    }
    /**
     * 删除会员公告
     */
    public function del()
    {
        $bullId = intval($_POST['data']);
        if (empty($bullId)) {
            $this->msg('会员公告ID无法获取！', false);
        }
        model('MemberBulletin')->delData($bullId);
        $this->msg('会员公告删除成功！');
    }
}