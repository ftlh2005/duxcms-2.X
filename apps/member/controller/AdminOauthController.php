<?php
/**
 * AdminOauthController.php
 * 登录设置管理
 * @author Life <349865361@qq.com>
 * @version 20140226
 */
class AdminOauthController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //列表信息
        $list = model('Oauth')->loadData();
        //模板赋值
        $this->assign('list', $list);
        $this->show();
    }
}