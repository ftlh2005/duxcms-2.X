<?php
/**
 * AdminMessageController.php
 * 系统短信管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminMessageController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND B.title LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminMessage/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'A.send_id = 0 AND A.send_system = 1 AND A.send_status = 0 '.$filterWhere;
        //系统短信列表信息
        $list = model('MemberMsg')->loadData($where, $limit);
        $count = model('MemberMsg')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加系统短信
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '发送');
        $this->show('adminmessage/info');
    }
    /**
     * 处理系统短信添加
     */
    public function addData()
    {
        $data = in($_POST);
        if (empty($data['username'])) {
            $this->msg('用户名或邮箱不能为空！', false);
        }
        if (empty($_POST['title'])) {
            $this->msg('公告标题未填写！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('公告内容未填写！', false);
        }
        //获取帐号信息
        $userInfo=model('Member')->getRepeat('username',$data['username']);
        if(empty($userInfo)){
            $userInfo=model('Member')->getRepeat('email',$data['username']);
        }
        if(empty($userInfo)){
            $this->msg('用户名输入不正确！', false);
        }
        $data['send_id'] = 0;
        $data['receive_id'] = $userInfo['user_id'];
        model('MemberMsg')->seadData($data, 1);
        $this->msg('系统短信添加成功！', 1);
    }
    /**
     * 编辑系统短信资料
     */
    public function edit()
    {
        $msgId = intval($_GET['msg_id']);
        if (empty($msgId)) {
            $this->msg('无法获取系统短信ID！', false);
        }
        //系统短信信息
        $info = model('MemberMsg')->getInfo('msg_id',$msgId);
        $hasInfo = model('MemberMsgHas')->getInfo('msg_id', $msgId);
        $user = model('Member')->getInfo($hasInfo['receive_id']);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '查看');
        $this->assign('user', $user);
        $this->show('adminmessage/read');
    }
    /**
     * 删除系统短信
     */
    public function del()
    {
        $msgId = intval($_POST['data']);
        if (empty($msgId)) {
            $this->msg('系统短信ID无法获取！', false);
        }
        $hasInfo = model('MemberMsgHas')->getInfo('msg_id', $msgId);
        $data = array();
        $data['send_status'] = 1;
        model('MemberMsgHas')->setData('has_id='.$hasInfo['has_id'], $data);
        $this->msg('短信删除成功！');
    }
}