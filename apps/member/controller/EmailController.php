<?php
/**
 * EmailController.php
 * 异步发送邮件
 * @author Life <349865361@qq.com>
 * @version 20140309
 */
class EmailController extends UserController
{
    /**
     * 发送邮件
     */
    public function send(){
        @ignore_user_abort(true); 
        @set_time_limit(0);
        $sysConfig=config('APP');
        if($_POST['safe_key']<>$sysConfig['SAFE_KEY']){
            return false;
        }
        $email=$_POST['email'];
        $title=$_POST['title'];
        $content=html_out($_POST['content']);
        //邮件配置
        $config=appConfig('member');
        $config=$config['APP_EMAIL'];
        $config['SMTP_HOST']=$config['SMTP_HOST'];
        $config['SMTP_PORT']=intval($config['SMTP_PORT']);
        $config['SMTP_SSL']=intval($config['SMTP_SSL']);
        $config['SMTP_AUTH']=intval($config['SMTP_AUTH']);
        $config['SMTP_USERNAME']=$config['SMTP_USERNAME'];
        $config['SMTP_PASSWORD']=$config['SMTP_PASSWORD'];
        $config['SMTP_FROM_TO']=$config['SMTP_USERNAME'];
        $config['SMTP_FROM_NAME']=$config['SMTP_FROM_NAME'];
        $config['SMTP_CHARSET']='utf-8';
        ob_start();
        Email::init($config);
        Email::send($email,$title,$content);
        $msg=ob_get_contents();
        ob_end_clean();
        if(!empty($msg)){
            return $msg;
        }else{
            return true;
        }
    }
}