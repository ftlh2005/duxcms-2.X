<?php
/**
 * AdminSettingController.php
 * 会员中心配置
 * @author Life <349865361@qq.com>
 * @version 20140312
 */
class AdminSettingController extends AdminController
{
    /**
     * 基本信息
     */
    public function index()
    {
        $config = appConfig('member');
        $info = $config['APP_SETTING'];
        $groupList = model('MemberGroup')->getTrueData();
        $this->assign('info', $info);
        $this->assign('groupList', $groupList);
        $this->show();
    }
    /**
     * 编辑配置信息
     */
    public function saveSettingData()
    {
        unset($_POST['site']);
        unset($_POST['relation_key']);
        $_POST['REG_STATUS'] = intval($_POST['REG_STATUS']);
        $data['APP_SETTING']=in($_POST);
        if( save_config('member', $data)){
            $this->msg('会员中心配置成功！');
        }else{
            $this->msg('会员中心配置失败，APP目录无写入权限！',0);
        }
    }
    /**
     * 邮箱配置
     */
    public function email()
    {
        $config = appConfig('member');
        $info = $config['APP_EMAIL'];
        $this->assign('info', $info);
        $this->show();
    }
    /**
     * 编辑配置信息
     */
    public function saveEmailData()
    {
        unset($_POST['site']);
        unset($_POST['relation_key']);
        $_POST['SMTP_PORT'] = intval($_POST['SMTP_PORT']);
        $_POST['SMTP_SSL'] = intval($_POST['SMTP_SSL']);
        $_POST['SMTP_AUTH'] = intval($_POST['SMTP_AUTH']);
        $data['APP_EMAIL']=in($_POST);
        if( save_config('member', $data)){
            $this->msg('邮件信息配置成功！');
        }else{
            $this->msg('邮件信息配置失败，APP目录无写入权限！',0);
        }
    }
    /**
     * 字段配置
     */
    public function fieldList()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= 'name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'form_id' => $formId,
            'keyword' => $filterKeyword,
        );
        $url = url('AdminSetting/fieldList', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //表单列表信息
        $list = model('MemberField')->loadData($where, $limit);
        $count = model('MemberField')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('typeField', api('duxcms','getFieldType'));
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加字段
     */
    public function fieldAdd()
    {
        //模板赋值
        $this->assign('action', 'Add');
        $this->assign('actionName', '添加');
        $this->assign('typeField', api('duxcms','getFieldType'));
        $this->assign('propertyField', api('duxcms','getFieldProperty'));
        $this->show('adminsetting/fieldinfo');
    }
    /**
     * 处理字段添加
     */
    public function fieldAddData()
    {
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('MemberField')->getInfoRepeat($_POST['field'])){
            $this->msg('字段不能重复！', false);
        }
        model('MemberField')->addData($_POST);
        $this->msg('字段添加成功！', 1);
    }
    /**
     * 编辑表单资料
     */
    public function fieldEdit()
    {
        $fieldId = intval($_GET['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        //表单信息
        $info = model('MemberField')->getInfo($fieldId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('typeField', api('duxcms','getFieldType'));
        $this->assign('propertyField', api('duxcms','getFieldProperty'));
        $this->show('adminsetting/fieldinfo');
    }
    /**
     * 处理表单资料
     */
    public function fieldEditData()
    {
        $fieldId = intval($_POST['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('MemberField')->getInfoRepeat($_POST['field'],$fieldId)){
            $this->msg('字段不能重复！', false);
        }
        model('MemberField')->saveData($_POST);
        $this->msg('字段修改成功！', 1);
    }
    /**
     * 删除字段
     */
    public function fieldDel()
    {
        $fieldId = intval($_POST['data']);
        if (empty($fieldId)) {
            $this->msg('字段ID无法获取！', false);
        }
        model('MemberField')->delData($fieldId);
        $this->msg('字段删除成功！');
    }
}