<?php
/**
 * AdminMemberRoleController.php
 * 角色管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminMemberRoleController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminMemberRole/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //角色列表信息
        $list = model('MemberRole')->loadData($where, $limit);
        $count = model('MemberRole')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //获取编辑接口
        $funList = hook_api('apiMemberRoleFun');
        $data=array();
        if(!empty($funList)){
            foreach ($funList as $app => $value) {
                if(!empty($value)&&is_array($value)){
                    foreach ($value as $key => $vo) {
                        $data[]=$vo;
                    }
                }
            }
        }
        $funList = array_order($data, 'order', 'asc');
        //模板赋值
        $this->assign('funList', $funList);
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加角色
     */
    public function add()
    {
        //模板赋值
        $this->assign('roleList', model('MemberRole')->loadData());
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminmemberrole/info');

    }
    /**
     * 处理角色添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('角色名称未填写！', false);
        }
        $_POST['base_purview'] = serialize($_POST['MemberPurvew']);
        $_POST['menu_purview'] = serialize($_POST['MemberMenu']);
        model('MemberRole')->addData($_POST);
        $this->msg('角色添加成功！', 1);
    }
    /**
     * 编辑角色资料
     */
    public function edit()
    {
        $roleId = intval($_GET['role_id']);
        if (empty($roleId)) {
            $this->msg('无法获取角色ID！', false);
        }
        //角色信息
        $info = model('MemberRole')->getInfo('role_id',$roleId);
        //模板赋值
        $this->assign('roleList', model('MemberRole')->loadData());
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminmemberrole/info');
    }
    /**
     * 处理角色资料
     */
    public function editData()
    {
        $data = in($_POST);
        $roleId = intval($_POST['role_id']);
        if (empty($roleId)) {
            $this->msg('无法获取角色ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('角色名称未填写！', false);
        }
        model('MemberRole')->saveData($_POST);
        $this->msg('角色修改成功！', 1);
    }
    /**
     * 编辑角色权限
     */
    public function purview()
    {
        $roleId = intval($_GET['role_id']);
        if (empty($roleId)) {
            $this->msg('无法获取角色ID！', false);
        }
        //角色信息
        $info = model('MemberRole')->getInfo('role_id',$roleId);
        $MemberPurvewArray = unserialize($info['base_purview']);
        $MemberMenuArray = unserialize($info['menu_purview']);
        //模板赋值
        $this->assign('MenberPurvew', model('MemberGroup')->getMemberPurview());
        $this->assign('MemberMenu', model('MemberMenu')->getMenuList(false));
        $this->assign('MemberPurvewArray', $MemberPurvewArray);
        $this->assign('MemberMenuArray', $MemberMenuArray);
        $this->assign('roleList', model('MemberRole')->loadData());
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminmemberrole/purview');
    }
    /**
     * 处理角色权限
     */
    public function purviewData()
    {
        $data = in($_POST);
        $roleId = intval($_POST['role_id']);
        if (empty($roleId)) {
            $this->msg('无法获取角色ID！', false);
        }
        $_POST['base_purview'] = serialize($_POST['MemberPurvew']);
        $_POST['menu_purview'] = serialize($_POST['MemberMenu']);
        model('MemberRole')->saveData($_POST);
        $this->msg('角色修改成功！', 1);
    }
    /**
     * 删除角色
     */
    public function del()
    {
        $roleId = intval($_POST['data']);
        if (empty($roleId)) {
            $this->msg('角色ID无法获取！', false);
        }
        model('MemberRole')->delData($roleId);
        $this->msg('角色删除成功！');
    }
}