<?php
/**
 * RegisterController.php
 * 会员注册
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class RegisterController extends UserController
{
    /**
     * 注册页面
     */
    public function index()
    {
        if(!$this->userConfig['REG_STATUS']){
            $this->alert('非常抱歉，当前会员注册已关闭！');
        }
    	$expand=model('MemberField')->getField();
    	$this->assign('expand', $expand);
        $this->showFrame();
    }
    /**
     * 处理注册数据
     */
    public function saveData()
    {
        if(!$this->userConfig['REG_STATUS']){
            $this->msg('非常抱歉，当前会员注册已关闭！',0);
        }
    	$data=in($_POST);
        if(!$data['agreement']){
			$this->msg('您未同意注册条款，请先仔细阅读后再同意注册！',0);
		}
		//表单验证
		if(!Check::userName($data['username'],5,20,'/^[x4E00-x9FA5\xf900-xfa2d\w\s]+$/u')||empty($data['username'])){
			$this->msg('用户名只支持5~20位字符，不能有特殊符号',0);
		}
		if(!Check::email($data['email'])||empty($data['email'])){
			$this->msg('邮箱地址输入不正确',0);
		}
		if(!Check::userName($data['password'],6,250,'/[\w\W]+/')||empty($data['password'])){
			$this->msg('密码最少需要6位字符',0);
		}
		if($data['password']<>$data['password2']){
            $this->msg('两次密码输入不同！',0);
        }
        $data['password'] = md5($data['password']);
        if(model('Member')->getRepeat('username',$data['username'])){
        	$this->msg('该用户名已被注册，请更换其他用户名！',0);
        }
        if(model('Member')->getRepeat('email',$data['email'])){
        	$this->msg('该邮箱已被注册，请更换其他邮箱！',0);
        }
        $verify = $_SESSION['verify'];
        unset($_SESSION['verify']);
        if($verify<>$data['verify']){
        	$this->msg('验证码输入错误或已经失效，请刷新验证码！',0);
        }
        $checkField=model('MemberField')->checkField($data);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }
        $data['group_id'] = $this->userConfig['REG_GROUP'];
        $data['user_id'] = model('Member')->addData($data);
        model('MemberData')->addData($data);
        $_SESSION[$this->appID.'_reg'] = $data['user_id'];
        //添加到动态
        $trendData = array();
        $trendData['user_id'] = $data['user_id'];
        $trendData['time'] = time();
        $trendData['content'] = date('Y年m月d日 H点i分s秒',$trendData['time']).' 注册帐号正式加入了我们！';
        model('MemberTrend')->addData($trendData);
        //判断是否需要绑定帐号
        $cookiePrefix = $this->appConfig['COOKIE_PREFIX'];
        //获取配置信息
        if(!empty($_SESSION[$this->cookiePrefix.'member_oauth_id'])&&!empty($_SESSION[$this->cookiePrefix.'member_oauth_app'])&&!empty($_SESSION[$this->cookiePrefix.'member_oauth_mark'])&&!empty($_SESSION[$this->cookiePrefix.'member_oauth_url'])){
            $oauthId = $_SESSION[$this->cookiePrefix.'member_oauth_id'];
            $oauthMark = $_SESSION[$this->cookiePrefix.'member_oauth_mark'];
            $oauthInfo = model('MemberOauth')->getInfo('open_id = "'.$oauthId.'" AND mark = "'.$oauthMark.'"');
            if(empty($oauthInfo)){
                $oauthData = array();
                $oauthData['user_id'] = $data['user_id'];
                $oauthData['open_id'] = $oauthId;
                $oauthData['mark'] = $oauthMark;
                model('MemberOauth')->addData($oauthData);
                unset($_SESSION[$this->cookiePrefix.'member_oauth_app']);
                unset($_SESSION[$this->cookiePrefix.'member_oauth_url']);
            }
            $this->msg('绑定并注册用户成功！', 1);
        }
        $this->msg('用户注册成功！', 1);
    }
    /**
     * 注册成功
     */
    public function success()
    {
    	$userId = intval($_SESSION[$this->appID.'_reg']);
    	if(empty($userId)){
    		$this->alert('当前会话已失效，请返回登录页面！',url('Login/index'));
    	}
    	$userInfo = model('Member')->getInfo($userId);
    	if(empty($userInfo)){
    		$this->alert('当前会话已失效，请返回登录页面！',url('Login/index'));
    	}
    	unset($_SESSION[$this->appID.'_reg']);
    	$this->assign('userInfo', $userInfo);
        $this->showFrame();

    }
    /**
     * 验证码
     */
    public function verify()
    {
    	Image::buildImageVerify();
    }
}