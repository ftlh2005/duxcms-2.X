<?php
/**
 * LoginController.php
 * 登录相关
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class LoginController extends UserController
{
    /**
     * 登录页面
     */
    public function index()
    {
        //获取登录接口
        $loginList = model('Oauth')->loadData();
        $data = array();
        if(!empty($loginList)){
            foreach ($loginList as $key => $value) {
                $config = appConfig($value['app']);
                $config = $config['OAUTH_'.$value['mark']];
                if($config['STATUS']){
                    $data[$key] = $value;
                }
            }
        }
        $this->assign('loginList', $data);
        $this->showFrame();
    }
    /**
     * 登录提交
     */
    public function postLogin()
    {
        $data=in($_POST);
        if(empty($data['username'])||empty($data['password'])){
            $this->msg('请填写完整登录信息!',0);
        }
        //获取帐号信息
        $info=model('Member')->getRepeat('username',$data['username']);
        if(empty($info)){
            $info=model('Member')->getRepeat('email',$data['username']);
        }
        //进行帐号验证
        if(empty($info)){
            $this->msg('登录失败! 您输入的帐号不存在!',0);
        }
        if($info['password']<>md5($data['password'])){
            $this->msg('登录失败! 您输入的帐号或密码错误!',0);
        }
        if($info['status']==0){
            $this->msg('登录失败! 您已被禁止登录!',0);
        }
        $data=array();
        $data['user_id']=$info['user_id'];
        $data['last_time']=time();
        $data['last_ip']=get_client_ip();
        model('Member')->saveData($data);
        //设置登录信息
        $cookie=$info['user_id'];
        if($data['remember']){
            $expire = time() + 604800;
        }else{
            $expire = time() + 7200;
        }
        $this->setLogin($cookie,$expire);
        if(is_ajax()){
            $this->msg('登录成功');
        }else{
            $this->redirect(url('member/Index/index'));
        }
        
    }
    /**
     * 退出登录
     */
    public function logout()
    {
        $this->clearLogin();
    }
    /**
     * AJAX获取登录信息
     */
    public function ajaxLoginInfo()
    {
        $userInfo = model('Member')->getCurrent();
        if($userInfo){
            $data = array();
            $data['username'] = $userInfo['username'];
            $data['group_name'] = $userInfo['group_name'];
            $this->msg($data);
        }else{
            $this->msg('未登录',false);
        }
    }
}