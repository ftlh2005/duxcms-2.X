<?php
/**
 * MessageController.php
 * 短消息管理
 * @author Life <349865361@qq.com>
 * @version 20140318
 */
class MessageController extends UserController
{
    /**
     * 列表页
     */
    public function index()
    {
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}'
        );
        $url = url('Message/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //筛选条件
        $type = intval($_GET['type']);
        switch ($type) {
            case 1:
                $where = 'A.receive_id = ' . $this->userId . ' AND A.send_id = 0 AND A.receive_status = 0';
                break;
            case 2:
                $where = 'A.send_id = ' . $this->userId . ' AND A.send_status = 0';
                break;
            default:
                $where = 'A.receive_id = ' . $this->userId . ' AND A.receive_status = 0';
                break;
        }
        //列表信息
        $list = model('MemberMsg')->loadData($where, $limit);
        $count = model('MemberMsg')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 查看短信息
     */
    public function info()
    {
        $msgId = intval($_GET['msg_id']);
        if (empty($msgId)) {
            $this->errorMsg();
        }
        $info = model('MemberMsg')->getInfo('msg_id', $msgId);
        if (empty($info)) {
            $this->errorMsg();
        }
        $type = intval($_GET['type']);
        $hasInfo = model('MemberMsgHas')->getInfo('msg_id', $msgId);
        if(empty($hasInfo)){
            $this->errorMsg();
        }
        if($type==2){
            //发信
            if($hasInfo['send_status']){
                $this->errorMsg();
            }
            if($hasInfo['send_id']<>$this->userId){
                $this->errorMsg();
            }
            $userInfo = model('Member')->getInfo($hasInfo['receive_id']);
            $typeName = '收信人';
        }else{
            //收信
            if($hasInfo['receive_status']){
                $this->errorMsg();
            }
            if($hasInfo['receive_id']<>$this->userId){
                $this->errorMsg();
            }
            if($hasInfo['send_id']){
                $userInfo = model('Member')->getInfo($hasInfo['send_id']);
            }
            $typeName = '发信人';
            //设置已读状态
            if(!$hasInfo['read_status']){
                $data = array();
                $data['read_status'] = 1;
                model('MemberMsgHas')->setData('has_id='.$hasInfo['has_id'], $data);
            }
        }

        //模板赋值
        $this->assign('info', $info);
        $this->assign('typeName', $typeName);
        $this->assign('userInfo', $userInfo);
        $this->show();
    }
    /**
     * 发送信息
     */
    public function send()
    {
        $this->show();
    }
    /**
     * 处理信息发送
     */
    public function sendData()
    {
        $data=in($_POST);
        $verify = $_SESSION['verify'];
        unset($_SESSION['verify']);
        if($verify<>$data['verify']){
            $this->msg('验证码输入错误或已经失效，请刷新验证码！',0);
        }
        if (empty($data['username'])) {
            $this->msg('用户名或邮箱不能为空！', false);
        }
        if (empty($data['title'])) {
            $this->msg('标题不能为空！', false);
        }
        if (empty($data['content'])) {
            $this->msg('内容不能为空！', false);
        }
        //获取帐号信息
        $userInfo=model('Member')->getRepeat('username',$data['username']);
        if(empty($userInfo)){
            $userInfo=model('Member')->getRepeat('email',$data['username']);
        }
        if(empty($userInfo)){
            $this->msg('用户名输入不正确！', false);
        }
        $data['send_id'] = $this->userId;
        $data['receive_id'] = $userInfo['user_id'];
        model('MemberMsg')->seadData($data);
        $this->msg('信息发送成功！', 1);
    }
    /**
     * 处理删除
     */
    public function del()
    {
        $id = in($_POST['id']);
        $type = intval($_POST['type']);
        if(empty($id)){
            $this->msg('请先选中要操作的信息！', false);
        }
        $idArray = explode(',', $id);
        foreach ($idArray as $value) {
            $hasInfo = model('MemberMsgHas')->getInfo('msg_id', $value);
            $data = array();
            if($type==2){
                //发信
                if($hasInfo['send_id']<>$this->userId||$hasInfo['send_status']==1){
                    continue;
                }
                
                $data['send_status'] = 1;
            }else{
                //收信
                if($hasInfo['receive_id']<>$this->userId||$hasInfo['receive_status']==1){
                    continue;
                }
                $data['receive_status'] = 1;
            }
            model('MemberMsgHas')->setData('has_id='.$hasInfo['has_id'], $data);
        }
        $this->msg('删除信息成功！', 1);
    }
    /**
     * 错误消息提示
     */
    public function errorMsg()
    {
        $this->msg('您要查看的短信不存在！', false);
    }
}