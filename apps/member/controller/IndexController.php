<?php
/**
 * IndexController.php
 * 会员首页
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class IndexController extends UserController
{
    /**
     * 首页面
     */
    public function index()
    {
        $userInfo = model('Member')->getInfo($this->userId);
        //快捷操作API接口
        $list = hook_api('apiMemberShortcut');
        $data = array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data = array_merge((array)$data,(array)$value);
            }
            $data = array_order($data, 'order', 'asc');
        }
        //未读信息
        $magNum = model('MemberMsg')->countData('A.receive_id = '.$this->userId.' AND read_status=0');
        //最新动态
        $trendList = model('MemberTrend')->loadData('user_id='.$this->userId, 5);
        //赋值
        $this->assign('userInfo', $userInfo);
        $this->assign('shortcutList', $data);
        $this->assign('trendList', $trendList);
        $this->assign('magNum', $magNum);
        $this->show();
    }
}