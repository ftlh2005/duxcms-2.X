<?php
/**
 * QLoginController.php
 * QQ登录
 * @author Life <349865361@qq.com>
 * @version 20140320
 */
class QLoginController extends UserController
{
    public function __construct(){
        parent::__construct();
        require_once(ROOT_PATH.'apps/member/lib/qq.php');
        $config = appConfig('member');
        $config = $config['OAUTH_QQ'];
        $this->appid=$config['ID'];
        $this->appkey=$config['KEY'];
        $this->scope='get_user_info,add_share';
        $this->callbackUrl='http://'.$_SERVER['HTTP_HOST'].url('member/QLogin/callback');
        $this->cookiePrefix = $this->appConfig['COOKIE_PREFIX'];
    }
    /**
     * 登录页面
     */
    public function index()
    {
        $qq=new qqPHP($this->appid, $this->appkey);
        $url=$qq->login_url($this->callbackUrl, $this->scope);
        $this->redirect($url);
    }
    /**
     * 登录回调
     */
    public function callback()
    {
        
        if(isset($_GET['code']) && trim($_GET['code'])!=''){
            $qq=new qqPHP($this->appid, $this->appkey);
            $result=$qq->access_token($this->callbackUrl, $_GET['code']);
            $token=$result['access_token'];
            $_SESSION[$this->cookiePrefix.'member_token']=$token;
        }
        //获取登录信息
        $qq=new qqPHP($this->appid, $this->appkey, $_SESSION[$this->cookiePrefix.'member_token']);
        $qq_oid=$qq->get_openid();
        $openid=$qq_oid['openid'];
        if(empty($openid)){
            $this->msg('系统无法获取您的登录信息！',0);
        }
        //查询是否已经存在
        $info = model('MemberOauth')->getInfo('open_id = "'.$openid.'" AND mark = "QQ"');
        $_SESSION[$this->cookiePrefix.'member_oauth_id']=$openid;
        $_SESSION[$this->cookiePrefix.'member_oauth_mark']='QQ';
        if(!empty($info)){
            $this->redirect(url('member/OauthLogin/loginData'));
        }else{
            //跳转到登录与绑定注册
            $_SESSION[$this->cookiePrefix.'member_oauth_app']=APP_NAME;
            $_SESSION[$this->cookiePrefix.'member_oauth_url']='member/QLogin';
            $this->redirect(url('member/OauthLogin/index'));
        }
    }
}