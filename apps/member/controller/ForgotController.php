<?php
/**
 * ForgotController.php
 * 密码找回
 * @author Life <349865361@qq.com>
 * @version 20140309
 */
class ForgotController extends UserController
{
    /**
     * 注册页面
     */
    public function index()
    {
        $this->showFrame();
    }
    /**
     * 处理注册数据
     */
    public function forgotData()
    {
    	$data=in($_POST);
		if(!Check::email($data['email'])||empty($data['email'])){
			$this->msg('邮箱地址输入不正确！',0);
		}
        $info = model('Member')->getRepeat('email',$data['email']);
        if(empty($info)){
        	$this->msg('邮箱地址输入不正确！',0);
        }
        $verify = $_SESSION['verify'];
        unset($_SESSION['verify']);
        if($verify<>$data['verify']){
        	$this->msg('验证码输入错误或已经失效，请刷新验证码！',0);
        }
        $time = intval($_SESSION[$this->appID.'_forgot_time']);
        if(!empty($time)){
            $newTime = $time+300;
            if($newTime>time()){
                $this->msg('您已经发送过验证邮件，请在5分钟后才能重新操作！',0);
            }
        }
        model('Forgot')->sendEmail($info);
        $_SESSION[$this->appID.'_forgot'] = $info['user_id'];
        $_SESSION[$this->appID.'_forgot_time'] = time();
        $this->msg('用户密码已经发送！', 1);
    }
    /**
     * 注册成功
     */
    public function success()
    {
    	$userId = intval($_SESSION[$this->appID.'_forgot']);
    	if(empty($userId)){
    		$this->alert('当前会话已失效，请返回登录页面！',url('Login/index'));
    	}
    	$userInfo = model('Member')->getInfo($userId);
    	if(empty($userInfo)){
    		$this->alert('当前会话已失效，请返回登录页面！',url('Login/index'));
    	}
    	unset($_SESSION[$this->appID.'_forgot']);
    	$this->assign('userInfo', $userInfo);
        $this->showFrame();

    }
}