<?php
/**
 * BulletinController.php
 * 内容管理
 * @author Life <349865361@qq.com>
 * @version 20140318
 */
class BulletinController extends UserController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $type = intval($_GET['type']);
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'type' => $type
        );
        $url = url('Bulletin/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //条件
        $userInfo = model('Member')->getInfo($this->userId);
        if ($type==1) {
            $where = ' FIND_IN_SET('.$userInfo['group_id'].',group_id) AND status = 1 ';
        }else{
            $where = '(FIND_IN_SET('.$userInfo['group_id'].',group_id) OR group_id = 0)  AND status = 1 ';
        }
        //列表信息
        $list = model('MemberBulletin')->loadData($where, $limit);
        $count = model('MemberBulletin')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('userInfo', $userInfo);
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 公告信息
     */
    public function info()
    {
        $bullId = intval($_GET['bull_id']);
        if (empty($bullId)) {
            $this->msg('您要查看的信息不存在！', false);
        }
        $info = model('MemberBulletin')->getInfo('bull_id', $bullId);
        if (empty($info)) {
            $this->msg('您要查看的信息不存在！', false);
        }
        $userInfo = model('Member')->getInfo($this->userId);
        if(!in_array($userInfo['group_id'], explode(',', $info['group_id']))&&$info['group_id']<>0){
            $this->msg('您要查看的信息不存在！', false);
        }
        //模板赋值
        $this->assign('info', $info);
        $this->show();
    }
}