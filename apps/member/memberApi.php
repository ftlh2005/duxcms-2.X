<?php
/**
 * memberApi.php
 * 会员中心
 * @author Life <349865361@qq.com>
 * @version 20140226
 */
class memberApi extends BaseApi
{
	/**
     * 菜单Api
     * 多维数组，第一维主菜单，第二维归类菜单，第三维菜单列表
     */
    public function apiAdminMenu()
    {
        return array(
            'member' => array(
                'name' => '会员',
                'order' => 5,
                'menu' => array(
                    'setting' => array(
                        'name' => '会员设置',
                        'order' => 0,
                        'menu' => array(
                            array(
                                'name' => '基本信息',
                                'url' => url('AdminSetting/index'),
                                'ico' => '&#xf013;',
                                'order' => 0
                            ),
                            array(
                                'name' => '邮箱设置',
                                'url' => url('AdminSetting/email'),
                                'ico' => '&#xf0e0;',
                                'order' => 1
                            ),
                            array(
                                'name' => '会员字段',
                                'url' => url('AdminSetting/fieldList'),
                                'ico' => '&#xf018;',
                                'order' => 2
                            ),
                            array(
                                'name' => '登录设置',
                                'url' => url('AdminOauth/index'),
                                'ico' => '&#xf110;',
                                'order' => 3
                            )
                        )
                    ),
                    'manage' => array(
                        'name' => '会员管理',
                        'order' => 1,
                        'menu' => array(
                            array(
                                'name' => '会员管理',
                                'url' => url('AdminMember/index'),
                                'ico' => '&#xf007;',
                                'order' => 0
                            ),
                            array(
                                'name' => '会员组管理',
                                'url' => url('AdminMemberGroup/index'),
                                'ico' => '&#xf0c0;',
                                'order' => 1
                            ),
                            array(
                                'name' => '角色管理',
                                'url' => url('AdminMemberRole/index'),
                                'ico' => '&#xf007;',
                                'order' => 2
                            )
                        )
                    ),
                    'module' => array(
                        'name' => '会员功能',
                        'order' => 2,
                        'menu' => array(
                            array(
                                'name' => '会员公告',
                                'url' => url('AdminBulletin/index'),
                                'ico' => '&#xf0f7;',
                                'order' => 0
                            ),
                            array(
                                'name' => '系统短信',
                                'url' => url('AdminMessage/index'),
                                'ico' => '&#xf075;',
                                'order' => 1
                            )
                        )
                    )
                )
            )
        );
    }
    /**
     * 会员菜单Api
     */
    public function apiMemberMenu()
    {
        return array(
            array(
                'name' => '会员中心',
                'order' => 0,
                'menu' => array(
                    array(
                        'name' => '会员首页',
                        'url' => url('Index/index'),
                        'order' => 0
                    ),
                    array(
                        'name' => '我的动态',
                        'url' => url('Trend/index'),
                        'order' => 1
                    ),
                    
                )
            ),
            array(
                'name' => '会员功能',
                'order' => 1,
                'menu' => array(
                    array(
                        'name' => '站内公告',
                        'url' => url('Bulletin/index'),
                        'order' => 1
                    ),
                    array(
                        'name' => '站内短信',
                        'url' => url('Message/index'),
                        'order' => 2
                    ),
                )
            ),
            array(
                'name' => '我的信息',
                'order' => 99,
                'menu' => array(
                    array(
                        'name' => '修改资料',
                        'url' => url('MemberSetting/info'),
                        'order' => 97
                    ),
                    array(
                        'name' => '设置头像',
                        'url' => url('MemberSetting/avatar'),
                        'order' => 98
                    ),
                    array(
                        'name' => '修改密码',
                        'url' => url('MemberSetting/password'),
                        'order' => 99
                    ),
                    array(
                        'name' => '安全退出',
                        'url' => url('Login/logout'),
                        'order' => 100
                    ),
                )
            )
        );
    }
    /**
     * 会员登录排除Api
     */
    public function apiMemberLoginExclude()
    {
        $noLogin = array(
            'member' => array(
                'Login' => array(
                    'index',
                    'postLogin',
                    'ajaxLoginInfo'
                ),
                'Register' => array(
                    'index',
                    'verify',
                    'saveData',
                    'success',
                ),
                'Forgot' => array(
                    'index',
                    'forgotData',
                    'success',
                ),
                'Email' => array(
                    'send',
                ),
                'QLogin' => array(
                    'index',
                    'callback',
                ),
                'OauthLogin' => array(
                    'checkSession',
                    'index',
                    'bindLogin',
                    'index',
                    'loginData',
                ),
            )
        );
        return $noLogin;
    }
    /**
     * 会员菜单缓存Api
     */
    public function apiCacheList()
    {
        return array(
            'member_menu'=>array(
                'name'=>'会员菜单缓存',
                'dir'=>'cache/app_cache/member_menu.php',
            ),
        );
    }
    /**
     * 会员权限Api
     * 当前APP权限控制，第一维控制器名称，第二维方法
     * @param string name 控制器描述
     * @param array auth 对象方法=>描述
     */
    public function apiMemberPurview()
    {
        return array(
            'Bulletin' => array(
                'name' => '会员公告',
                'auth' => array(
                    'index' => '列表',
                    'info' => '详情',
                )
            ),
            'MemberSetting' => array(
                'name' => '会员资料',
                'auth' => array(
                    'info' => '基本资料',
                    'avatar' => '头像设置',
                    'password' => '密码修改'
                )
            ),
            'Message' => array(
                'name' => '站内短信',
                'auth' => array(
                    'index' => '列表',
                    'info' => '详情',
                    'send' => '发信息',
                    'del' => '删除',
                )
            )
        );
    }
    /**
     * 会员快捷操作接口
     * @return array 接口信息
     */
    public function apiMemberShortcut()
    {
        return array(
            array(
                'name'=>'我的动态',
                'url'=>url('Trend/index'),
                'ico'=>'&#xf086;',
                'order'=>0,
                ),
            array(
                'name'=>'站内短信',
                'url'=>url('Message/index'),
                'ico'=>'&#xf003;',
                'order'=>98,
                ),
            array(
                'name'=>'修改资料',
                'url'=>url('memberSetting/info'),
                'ico'=>'&#xf007;',
                'order'=>99,
                ),
            );
    }
    /**
     * 会员登录接口
     * @return array 接口信息
     */
    public function apiMemberOauth()
    {
        return array(
            array(
                'name' => 'QQ帐号登录',
                'url' => url('QLogin/index'),
                'mark' => 'QQ',
                'order' => 0,
                ),
            );
    }
    /**
     * 获取会员菜单
     * @return array 会员中心基本信息
     */
    public function getMenuList($data)
    {
        return model('MemberMenu')->getMenuList($data['purview'],$data['user_id']);
    }
    /**
     * 获取当前登录会员信息
     * @param array 信息数组
     * @param int 是否系统信息
     */
    public function getCurrentMember()
    {
        return model('Member')->getCurrent();
    }
    /**
     * 查找会员信息
     * @return array 会员基本信息
     */
    public function getMemberInfoSearch($data)
    {
        return model('Member')->getRepeat($data['name'], $data['data']);
    }
    /**
     * 获取会员信息
     * @return array 会员基本信息
     */
    public function getMemberInfo($data)
    {
        return model('Member')->getInfo($data['user_id']);
    }
    /**
     * 获取会员组信息
     * @return array 会员组基本信息
     */
    public function getGroupInfo($data)
    {
        return model('MemberGroup')->getInfo('group_id', $data['group_id']);
    }
    /**
     * 获取角色信息
     * @return array 角色基本信息
     */
    public function getRoleInfo($data)
    {
        return model('MemberRole')->getInfo('role_id', $data['role_id']);
    }
    /**
     * 获取会员列表
     * @return array 会员组基本信息
     */
    public function getGroupList($data)
    {
        return model('MemberGroup')->loadData($data['where'],$data['limit']);
    }
    /**
     * 获取会员组树
     * @return array 会员组基本信息
     */
    public function getGroupTrue($data)
    {
        return model('MemberGroup')->getTrueData($data['where'],$data['group_id']);
    }
    /**
     * 增加会员动态
     * @param array 信息数组
     */
    public function addMemberTrend($data)
    {
        return model('MemberTrend')->addData($data);
    }
    /**
     * 发送短信息
     * @param array 信息数组
     * @param int 是否系统信息
     */
    public function MemberMsg($data)
    {
        return model('MemberMsg')->seadData($data['data'], $data['system']);
    }

    

}