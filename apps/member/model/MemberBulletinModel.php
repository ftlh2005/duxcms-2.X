<?php
/**
 * MemberBulletinModel.php
 * 会员动态表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberBulletinModel extends BaseModel
{
    protected $table = 'member_bulletin';
    /**
     * 获取列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->select($condition, '', 'bull_id DESC', $limit);
    }
    /**
     * 获取总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 获取动态
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 动态
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 添加动态
     * @param array $data 动态
     * @return int ID
     */
    public function addData($data)
    {
        $data = $this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 保存动态
     * @param array $data 动态
     * @return bool 状态
     */
    public function saveData($data)
    {
        $data = $this->foramtData($data);
        return $this->update('bull_id=' . $data['bull_id'], $data);
    }
    /**
     * 删除动态
     * @param int $msgId ID
     * @return bool 状态
     */
    public function delData($bullId)
    {
        return $this->delete('bull_id=' . $bullId);
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $bullId=intval($data['bull_id']);
        $data['time']=strtotime($data['time']);
        if(empty($data['time'])){
            $data['time']=time();
        }
        $data['content'] = html_in($data['content']);
        if(in_array("0", $data['group_id'])){
            $data['group_id'] = 0;
        }else{
            $data['group_id'] = implode(',', $data['group_id']);
        }
        
        return $data;
    }
}