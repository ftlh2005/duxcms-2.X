<?php
/**
 * MemberTrendModel.php
 * 会员动态表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberTrendModel extends BaseModel
{
    protected $table = 'member_trend';
    /**
     * 获取列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->select($condition, '', 'trend_id DESC', $limit);
    }
    /**
     * 获取总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 获取信息
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 信息
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 添加信息
     * @param array $data 信息
     * @return int ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
}