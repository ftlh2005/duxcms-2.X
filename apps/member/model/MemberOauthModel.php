<?php
/**
 * MemberOauthModel.php
 * 登录表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberOauthModel extends BaseModel
{
    protected $table = 'member_oauth';
    /**
     * 获取信息
     * @param string $where 查询条件
     * @return array 信息
     */
    public function getInfo($where)
    {
        return $this->find($where);
    }
    /**
     * 添加信息
     * @param array $data 信息
     * @return int ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
}