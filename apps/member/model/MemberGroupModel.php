<?php
/**
 * MemberGroupModel.php
 * 会员动态表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberGroupModel extends BaseModel
{
    protected $table = 'member_group';
    /**
     * 获取列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 0)
    {
        return $this->select($condition, '', 'sequence ASC,group_id DESC', $limit);
    }
    /**
     * 获取总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 获取动态
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 动态
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 添加动态
     * @param array $data 动态
     * @return int ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存动态
     * @param array $data 动态
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('group_id=' . $data['group_id'], $data);
    }
    /**
     * 删除动态
     * @param int $msgId ID
     * @return bool 状态
     */
    public function delData($groupId)
    {
        return $this->delete('group_id=' . $groupId);
    }
    /**
     * 获取功能权限
     * @return array 状态
     */
    public function getMemberPurview()
    {
        return hook_api('apiMemberPurview');
    }
    /**
     * 获取用户组树
     * @param array $condition 条件
     * @return array 栏目树列表
     */
    public function getTrueData($condition = null, $groupId=0)
    {
        $data=$this->select($condition, '', 'sequence ASC,group_id ASC');
        $cat = new Category(array('group_id', 'parent_id', 'name', 'cname'));
        $data=$cat->getTree($data, intval($groupId));
        return $data;
    }
}