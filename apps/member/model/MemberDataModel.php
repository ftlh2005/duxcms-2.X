<?php
/**
 * MemberDataModel.php
 * 会员扩展表操作
 * @author Life <349865361@qq.com>
 * @version 20140315
 */
class MemberDataModel extends BaseModel
{
    protected $table = 'member_data';
    /**
     * 获取扩展信息
     * @param int $userId 会员ID
     * @return array 扩展信息
     */
    public function getInfo($userId)
    {
        return $this->find('user_id=' . $userId);
    }
    /**
     * 添加用户扩展信息
     * @param array $data 用户信息
     * @return int 用户ID
     */
    public function addData($data)
    {
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 修改用户扩展信息
     * @param array $data 用户信息
     * @return int 用户ID
     */
    public function saveData($data)
    {
        if(!$this->getInfo($data['user_id'])){
            $this->addData($data);
        }
        $data=$this->foramtData($data);
        return $this->update('user_id=' . $data['user_id'], $data);
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $newData=array();
        $newData['user_id']=intval($data['user_id']);
        $fieldList=model('MemberField')->loadData();
        if(empty($fieldList)||!is_array($fieldList)){
            return $newData;
        }
        foreach ($fieldList as $value) {
            $newData[$value['field']]=api('duxcms','getFieldForamt',array('data'=>$data[$value['field']],'type'=>$value['type']));
        }
        return $newData;
    }

}