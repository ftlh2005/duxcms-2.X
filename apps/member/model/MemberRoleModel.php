<?php
/**
 * MemberRoleModel.php
 * 会员动态表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberRoleModel extends BaseModel
{
    protected $table = 'member_role';
    /**
     * 获取列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 0)
    {
        return $this->select($condition, '', 'role_id ASC', $limit);
    }
    /**
     * 获取总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 获取动态
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 动态
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 添加动态
     * @param array $data 动态
     * @return int ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存动态
     * @param array $data 动态
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('role_id=' . $data['role_id'], $data);
    }
    /**
     * 删除动态
     * @param int $msgId ID
     * @return bool 状态
     */
    public function delData($roleId)
    {
        return $this->delete('role_id=' . $roleId);
    }
}