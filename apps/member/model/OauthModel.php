<?php
/**
 * OauthModel.php
 * 登录接口获取
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class OauthModel extends BaseModel
{
    /**
     * 获取Api列表
     * @return array 列表数据
     */
    public function loadData()
    {
        $list=hook_api('apiMemberOauth');
        $data=array();
        if(!empty($list)){
            $i=0;
            foreach ($list as $app => $value) {
                if(!empty($value)&&is_array($value)){
                    foreach ($value as $key => $vo) {
                        $i++;
                        $data[$i]=$vo;
                        $data[$i]['app']=$app;
                    }
                }
            }
        }
        return array_order($data, 'order', 'asc');
        return $data;
    }
    /**
     * 获取模型名称
     * @return array 模型列表名称
     */
    public function getName()
    {
        $list=hook_api('apiContentModel');
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value['name'];
            }
        }
        return $data;
    }
}