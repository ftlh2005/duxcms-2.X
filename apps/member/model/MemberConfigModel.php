<?php
/**
 * MemberConfigModel.php
 * 会员中心配置表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberConfigModel extends BaseModel
{
    protected $table = 'member_config';
    /**
     * 站点配置信息
     * @return array 配置数组
     */
    public function getInfo()
    {
        $list = $this->select();
        $data = array();
        foreach ($list as $key => $value) {
            $data[$value['name']] = $value['data'];
        }
        return $data;
    }
    /**
     * 保存站点配置信息
     * @param array $data 站点配置信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('site_id=' . $data['site_id'], $data);
    }
}