-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 01 月 27 日 21:37
-- 服务器版本: 5.6.13
-- PHP 版本: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 转存表中的数据 `dux_attachment`
--

INSERT INTO `dux_attachment` (`file_id`, `relation_id`, `url`, `original`, `title`, `ext`, `size`, `time`, `model`, `relation_key`) VALUES
(2, 5, '/upload/2014-01/24/8b77e484f2e3d27b808f3a7d0f85bd22.jpg', '/upload/2014-01/24/8b77e484f2e3d27b808f3a7d0f85bd22.jpg', 'U8958P1T1D29335542F23DT20140124161119', 'jpg', 48973, 1390553831, 'content_atricle', '1390553723714'),
(1, 5, '/upload/2014-01/24/c0ab0cfb82a461a940031691c26c8d5a.jpg', '/upload/2014-01/24/c0ab0cfb82a461a940031691c26c8d5a.jpg', 'U8958P1T1D29335542F21DT20140124161119', 'jpg', 53549, 1390553812, 'content_atricle', '1390553723714'),
(3, 5, '/upload/2014-01/24/a2bf8983e4174cfd0b2cbe73d7d8fe9c.jpg', '/upload/2014-01/24/a2bf8983e4174cfd0b2cbe73d7d8fe9c.jpg', 'U8958P1T1D29335542F1394DT20140124161119', 'jpg', 44744, 1390553846, 'content_atricle', '1390553723714'),
(4, 5, '/upload/2014-01/24/0d8a7748d11baf35f8ea730f7d1f1b96.jpg', '/upload/2014-01/24/0d8a7748d11baf35f8ea730f7d1f1b96.jpg', 'U8958P1T1D29335542F1395DT20140124161119', 'jpg', 45535, 1390553861, 'content_atricle', '1390553723714'),
(5, 6, '/upload/2014-01/24/82b3e7e474418e4eacc5d13c750f2451.jpg', '/upload/2014-01/24/82b3e7e474418e4eacc5d13c750f2451.jpg', '1390489813_lUGnCp', 'jpg', 53914, 1390553966, 'content_atricle', '1390553934146');

-- --------------------------------------------------------
--
-- 转存表中的数据 `dux_catalog`
--

INSERT INTO `dux_catalog` (`catalog_id`, `parent_id`, `class_id`, `url`, `sequence`, `show`, `name`, `site`) VALUES
(1, 0, 1, '', 1, 1, NULL, 1),
(5, 1, 5, '', 1, 1, '', 1),
(6, 1, 6, NULL, 2, 1, NULL, 1),
(9, 0, 9, '', 5, 1, NULL, 1),
(10, 0, 0, 'http://www.duxcms.com', 6, 1, 'Duxcms官网', 1);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_category`
--

INSERT INTO `dux_category` (`class_id`, `parent_id`, `app`, `sequence`, `name`, `urlname`, `image`, `class_tpl`, `keywords`, `description`,  `fieldset_id`, `site`) VALUES
(1, 0, 'article', 1, '新闻演示', 'guoneixinwen', '', 'list.html', '', '', 0, 1),
(5, 1, 'article', 1, '内地新闻', 'neidixinwen', '', 'list.html', '', '', 0, 1),
(6, 1, 'article', 1, '港澳台新闻', 'gangaotaixinwen', '', 'list.html', '', '', 0, 1),
(9, 0, 'pages', 1, '单页演示', 'guanyuwomen', '', 'page.html', '', '', 0, 1);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_category_article`
--

INSERT INTO `dux_category_article` (`class_id`, `type`, `content_tpl`, `config_upload`, `content_order`, `page`) VALUES
(1, 0, 'content.html', 'a:17:{s:22:"THUMBNAIL_TYPE_INHERIT";i:1;s:14:"THUMBNAIL_TYPE";s:1:"0";s:20:"THUMBNAIL_ON_INHERIT";i:1;s:12:"THUMBNAIL_ON";s:1:"0";s:22:"THUMBNAIL_SIZE_INHERIT";i:1;s:15:"THUMBNAIL_WIDTH";s:3:"500";s:16:"THUMBNAIL_HEIGHT";s:3:"500";s:20:"WATERMARK_ON_INHERIT";i:1;s:12:"WATERMARK_ON";s:1:"0";s:26:"WATERMARK_POSITION_INHERIT";i:1;s:18:"WATERMARK_POSITION";s:1:"0";s:23:"WATERMARK_IMAGE_INHERIT";i:1;s:15:"WATERMARK_IMAGE";s:8:"logo.png";s:19:"UPLOAD_TYPE_INHERIT";i:1;s:11:"UPLOAD_TYPE";s:15:"jpg,gif,png,bmp";s:19:"UPLOAD_SIZE_INHERIT";i:1;s:11:"UPLOAD_SIZE";s:1:"3";}', 'time DESC', 10),
(5, 1, 'content.html', 'a:17:{s:22:"THUMBNAIL_TYPE_INHERIT";i:1;s:14:"THUMBNAIL_TYPE";s:1:"0";s:20:"THUMBNAIL_ON_INHERIT";i:1;s:12:"THUMBNAIL_ON";s:1:"0";s:22:"THUMBNAIL_SIZE_INHERIT";i:1;s:15:"THUMBNAIL_WIDTH";s:3:"500";s:16:"THUMBNAIL_HEIGHT";s:3:"500";s:20:"WATERMARK_ON_INHERIT";i:1;s:12:"WATERMARK_ON";s:1:"0";s:26:"WATERMARK_POSITION_INHERIT";i:1;s:18:"WATERMARK_POSITION";s:1:"0";s:23:"WATERMARK_IMAGE_INHERIT";i:1;s:15:"WATERMARK_IMAGE";s:8:"logo.png";s:19:"UPLOAD_TYPE_INHERIT";i:1;s:11:"UPLOAD_TYPE";s:15:"jpg,gif,png,bmp";s:19:"UPLOAD_SIZE_INHERIT";i:1;s:11:"UPLOAD_SIZE";s:1:"3";}', 'time DESC', 10),
(6, 1, 'content.html', 'a:17:{s:22:"THUMBNAIL_TYPE_INHERIT";i:1;s:14:"THUMBNAIL_TYPE";s:1:"0";s:20:"THUMBNAIL_ON_INHERIT";i:1;s:12:"THUMBNAIL_ON";s:1:"0";s:22:"THUMBNAIL_SIZE_INHERIT";i:1;s:15:"THUMBNAIL_WIDTH";s:3:"500";s:16:"THUMBNAIL_HEIGHT";s:3:"500";s:20:"WATERMARK_ON_INHERIT";i:1;s:12:"WATERMARK_ON";s:1:"0";s:26:"WATERMARK_POSITION_INHERIT";i:1;s:18:"WATERMARK_POSITION";s:1:"0";s:23:"WATERMARK_IMAGE_INHERIT";i:1;s:15:"WATERMARK_IMAGE";s:8:"logo.png";s:19:"UPLOAD_TYPE_INHERIT";i:1;s:11:"UPLOAD_TYPE";s:15:"jpg,gif,png,bmp";s:19:"UPLOAD_SIZE_INHERIT";i:1;s:11:"UPLOAD_SIZE";s:1:"3";}', 'time DESC', 10);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_category_page`
--

INSERT INTO `dux_category_page` (`class_id`, `content`) VALUES
(9, '单页演示');

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_content`
--

INSERT INTO `dux_content` (`content_id`, `class_id`, `title`, `urltitle`, `font_color`, `font_bold`, `keywords`, `description`, `time`, `image`, `url`, `sequence`, `status`, `copyfrom`, `views`, `taglink`, `tpl`, `site`) VALUES
(1, 5, '广西女子因收货迟到称“炸快递公司”被拘留', 'guangxinvziyinshouhuochidaoche', NULL, 0, '快递公司,黎某,柳州', '中新网柳州1月22日电 (谢平 秦付林)广西柳州警方22日通报，一女子在柳州网购后，因快递公司的人员未及时送货，在腾讯微博上发布“扛炸药包炸快递公司”的言论，被警方拘留。据警方介', 1390387901, '', '', 0, 1, 'duxcms', 132, 1, '', 1),
(2, 5, '国家森防指紧急启动森林火灾Ⅳ级应急响应', 'guojiasenfangzhijinjiqidongsen', NULL, 0, '森林火灾,应急响应,紧急', '新华网北京1月24日电 (记者刘羊旸)针对当前严峻的森林防火形势，中国国家森林防火指挥部24日紧急启动森林火灾Ⅳ级应急响应。据国家森防指办公室消息，1月23日，中国湖南、江西、贵州', 1390553607, '', '', 0, 1, 'duxcms', 11, 1, '', 1),
(3, 5, '上海今年将有万台出租车提供免费WiFi', 'shanghaijinnianjiangyouwantaic', NULL, 0, '出租车,上海,WiFi', '新华网上海1月24日电 (周琳、施文)移动户外新媒体公司iTaxi Media近日在上海宣布，2014年第二季度上海将有超过10000台出租车提供免费WiFi热点覆盖，乘客只需要在首次使用时进行简单的注册，', 1390553652, '', '', 0, 1, 'duxcms', 86, 1, '', 1),
(4, 5, '北京拟建人口资源环境监测体系 测外来人口增速', 'beijingnijianrenkouziyuanhuanj', NULL, 0, '相结合,王文杰,北京,常住,监测', '法制晚报讯(记者 赵颖彦)今年，本市将建立人口资源环境监测体系，从人口分布、就业形势等方面对城市承受力进行“把脉”。其中，将着重对外来人口增速进行测算。据了解，该体系将在', 1390553683, '', '', 0, 1, 'duxcms', 2, 1, '', 1),
(5, 5, '北京朝阳8万平方违建开拆 4000多租户搬离(图)', 'beijingchaoyang8wanpingfangwei', NULL, 0, '万平,违建,租户', '2014年1月23日上午9时许，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除，4000余位租住在这些“违建群”里的住户搬离。图片来源：法制晚报林晖/CFP', 1390553723, '/upload/2014-01/24/c0ab0cfb82a461a940031691c26c8d5a.jpg', '', 0, 1, 'duxcms', 8, 1, '', 1),
(6, 5, '习近平念两幅春联向党外人士致以新春祝福(图)', 'xijinpingnianliangfuchunlianxi', NULL, 0, '习近平,春联,党外人士,2014年春节', '1月22日，习近平、俞正声、张高丽等在北京人民大会堂同党外人士欢聚一堂，共迎新春。 新华社记者鞠鹏摄习近平同党外人士共迎新春代表中共中央，向各民主党派、工商联和无党派人士，', 1390553933, '/upload/2014-01/24/82b3e7e474418e4eacc5d13c750f2451.jpg', '', 0, 1, 'duxcms', 13, 1, '', 1),
(7, 6, '香港机场特警更换枪袋走火无人受伤', 'xianggangjichangtejinggenghuan', NULL, 0, '机场特警,佩枪走火,特警,走火', '中新网1月24日电 据香港《文汇报》报道，香港机场警署23日早晨发生警员佩枪走火事件。一名机场特警在机场警署更衣室内更换枪袋期间，疑在拔出Glock-17曲尺手枪时突然走火惊动同袍，幸', 1390554810, '', '', 0, 1, 'duxcms', 26, 1, '', 1),
(8, 6, '梁振英：香港人思想开放愿意接受外来人', 'liangzhenyingxianggangrensixia', NULL, 0, '梁振英,香港文化,外来人,明报,香港特首', '中新网1月24日电 据香港《明报》网站消息，正在福建访问的香港特首梁振英，今早(24日)出席闽港经贸合作交流会。他在会上致辞称，港人思想开放，愿意接受外来人。他在致辞时说，香港', 1390554846, '', '', 0, 1, 'duxcms', 8, 1, '', 1),
(9, 5, '成渝动车正测试列车WiFi系统 有望不久后投用', 'chengyudongchezhengceshilieche', NULL, 0, 'wifi,动车', '小伙列车上卖WiFi引发乘客对列车WiFi的向往成都商报记者 张舒实习生 朱茜西核心提示铁路通信信号方面的专家介绍，铁路上火车内的WiFi覆盖仍是取决于运营商的地面信号覆盖，铁路线路难', 1390569170, '', '', 0, 1, 'duxcms', 34, 1, '', 1);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_content_article`
--

INSERT INTO `dux_content_article` (`content_id`, `content`) VALUES
(1, '&lt;p style=&quot;font-size:14px;color:#333333;font-family:宋体;background-color:#FFFFFF;text-indent:2em;&quot;&gt;\r\n	中新网柳州1月22日电 (谢平 秦付林)广西柳州警方22日通报，一女子在柳州网购后，因快递公司的人员未及时送货，在腾讯微博上发布“扛炸药包炸快递公司”的言论，被警方拘留。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;font-size:14px;color:#333333;font-family:宋体;background-color:#FFFFFF;text-indent:2em;&quot;&gt;\r\n	据警方介绍，近日，一名网名叫“李昕”的网友在腾讯微博上发布：“柳州某某快递真可恶，再不把购买的东西给我送来，我扛上炸药包把你们公司炸了”的言论。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;font-size:14px;color:#333333;font-family:宋体;background-color:#FFFFFF;text-indent:2em;&quot;&gt;\r\n	经询问，网民“李昕”真实姓名黎某，广西河池市宜州市人。黎某承认，其数日前在网上购买了一批化妆品，收货地址是柳州市城中区雅儒路的亲戚家。当黎某从宜州市赶到柳州市接货时，快递公司的人员却迟迟没有将货送上门，其一气之下便在个人的腾讯微博上发表了要炸柳州某某快递公司的言论。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;font-size:14px;color:#333333;font-family:宋体;background-color:#FFFFFF;text-indent:2em;&quot;&gt;\r\n	警方认定，黎某通过公开的方式表示个人的不满情绪，主观是故意的，扰乱了正常的公共秩序。柳州市公安局中南派出所根据《中华人民共和国治安管理处罚法》之规定，给予黎某行政拘留三日的处罚。\r\n&lt;/p&gt;\r\n&lt;p align=&quot;right&quot; style=&quot;font-size:14px;color:#333333;font-family:宋体;background-color:#FFFFFF;text-indent:2em;&quot;&gt;\r\n	(原标题：广西女子因收货迟到发微博“炸快递公司”被拘留)\r\n&lt;/p&gt;'),
(2, '&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	新华网北京1月24日电 (记者刘羊旸)针对当前严峻的森林防火形势，中国国家森林防火指挥部24日紧急启动森林火灾Ⅳ级应急响应。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	据国家森防指办公室消息，1月23日，中国湖南、江西、贵州、广东、广西等省区相继发生46起森林火灾，截至24日9时，已扑灭27起，仍有19起正在扑救中。其中，贵州省黔南州福泉市森林火灾造成5人死亡1人受伤，广西桂林市资源县森林火灾造成6人受伤。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	对此，国家森防指高度重视，立即召集有关部门研究应对措施，并派出工作组赶赴福泉火场协调指导火灾扑救工作。国家森防指要求有关省区组织精干力量，尽快扑灭火灾；严格落实有火必报和卫星热点核查“零报告”，确保信息畅通；进一步明确扑火目标，扑火过程中要科学指挥，坚持以人为本，最大限度地降低火灾损失。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	今年以来，中国南方大部分省区降水持续偏少，旱情严重，森林火险等级居高不下，森林防火形势十分严峻。据统计，截至1月23日，国家森防指累计监测卫星热点3538个，反馈林火178起，与历年同期均值相比，均大幅度增加。对此，国家森防指特召开南方省区视频调度会，并连续下发紧急通知，对当前和春节期间防火工作进行安排部署。\r\n&lt;/p&gt;'),
(3, '&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	新华网上海1月24日电 (周琳、施文)移动户外新媒体公司iTaxi Media近日在上海宣布，2014年第二季度上海将有超过10000台出租车提供免费WiFi热点覆盖，乘客只需要在首次使用时进行简单的注册，以后就能在打车时通过无线热点高速上网，并且完全免费。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	目前，iTaxi Media公司已经与上海五大出租车集团公司之一的蓝色联盟达成合作，蓝色联盟旗下的5600辆出租车将率先全线开通WiFi功能。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	据了解，出租车之所以能够提供免费WiFi，靠的是iTaxi Media公司和飞天通信科技联合研发的全新iTaxi 3G智能触屏。与其他安装于副驾驶座后方的广告屏不同，iTaxi 3G智能触屏搭载有3G通讯模块，并能够将3G信号通过WiFi的方式分享。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;div&gt;\r\n	&lt;br /&gt;\r\n&lt;/div&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	iTaxi 3G智能屏在给乘客提供免费WiFi的同时，也给广告商提供了全新的营销思路。iTaxi Media公司总裁梁民介绍，乘客之前乘坐出租车的时间平均为15分钟，由于大城市交通拥堵升级，这一时间目前已经达到22分钟。“逐年递增的出租车车行时间提供了一个优质的户外广告投放环境，出租车内模庞大的城市高消费人群更是品牌最理想的目标消费群。”梁民说。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	依靠高速的3G网络，原本更新困难的出租车广告屏如今可以实现实时更新广告内容。再加上GPS定位系统的介入，iTaxi的3G智能屏还能按需选定目标区域、目标时间以及目标广告周期进行制定推送，帮助广告主实现精准营销。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	凭借着免费高速的WiFi热点覆盖，原本只依靠单向度广告推送的出租车广告屏还实现了与个人移动终端的全面互动。乘客能够及时将乘车过程中感兴趣的内容，比如热门APP、网络小说、流行音乐、优惠打折券等，以应用下载、二维码扫描、微信分享的方式从大屏迅速下载至小屏，便于下车随身携带。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	据了解，iTaxi Media预计将在2014年把3G智能屏拓展到全国5个以上一线、二线城市，并于年底前在全国覆盖超过3万台出租车。\r\n&lt;/p&gt;'),
(4, '&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	法制晚报讯(记者 赵颖彦)今年，本市将建立人口资源环境监测体系，从人口分布、就业形势等方面对城市承受力进行“把脉”。其中，将着重对外来人口增速进行测算。据了解，该体系将在三年内完成。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	今天上午，北京市统计局、国家统计局北京调查总队召开全市统计工作会议，并发布《2014年北京市统计工作报告》(以下简称“报告”)。《报告》中指出，2014年本市将优化经济增长目标监测体系，改革国民经济核算体系。此外，还将建立京、津、冀区域协调发展跟踪体系。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	分析承载能力 提出城市规划建议\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	2004年本市常住人口为1492.7万人，2013年此项数字增至2114.8万人，10年间本市常住人口增加了622.1万人，其中常住外来人口增加472.9万人。按照数据平均计算，本市常住外来人口每天增加1296人。外来人口增加已成为北京人口快速增长的“主力”。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	由此，今年本市将建立人口资源环境监测体系。市统计局局长王文杰指出，届时将从人口分布、就业形势、功能定位、产业布局、能耗与经济贡献等角度，按照人口、资源、环境与产业结构升级相结合、与区域空间布局相结合、与城市承载能力相结合的要求，全面分析区域承受力情况及人口、资源、环境协调发展存在的问题，有针对性地提出合理规划、科学引导的对策和建议。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	据市统计局人口就业处相关负责人介绍，此前人口、资源和环境是相对独立的统计项目，今年起将通过整合来共同搭建一个大数据平台。例如，产业布局和人口就业之间的联系，资源发展与人口增速之间的关联，今后都将在这一体系中体现。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	王文杰指出，人口资源环境监测体系中，将增加一项重要内容便是常住外来人口的测算，尤其是常住外来人口的增速问题。由于常住外来人口也对北京城市发展做出了贡献，因此对这部分人口的研究也值得关注。人口就业处相关负责人表示，这一体系将在三年内建成。届时将以数据说话，有针对性地对城市发展提出合理规划。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	减少经济增长监测指标\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	《报告》中指出，今年还将建立“经济增长目标监测体系”，其中将突出反映经济运行态势和经济增长质量、效益的指标，删减非直接支撑经济增长的指标。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;div&gt;\r\n	&lt;br /&gt;\r\n&lt;/div&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	王文杰表示，此前该指标共20项，今年将减少7项。“瘦身”后，经济增长目标监测将从此前重速度重数量，变为从生产和需求的角度反映宏观经济运行态势，反映经济增长的质量和效益，发挥及时监测、及时预警、防范风险的作用。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	京津冀三地数据将共享\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	随着区域融合，北京周边“小伙伴”的环境、人口也间接对北京产生了影响。今年，本市将建立京、津、冀区域协调发展跟踪监测体系。《报告》中提出，将建立跟踪监测协调机构，制定三地数据整合和共享方案，更加主动地推动京津同城化、京津冀一体化和环渤海地区协调发展。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	其中，将紧密围绕人口分布、综合实力、产业分工与合作、对外经济、基础设施布局和生态环境等重要因素，建立跟踪监测体系，开展统计监测与评价研究，为破解区域协调发展中的难题提供决策依据。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	(原标题：人口资源 环境监测体系 三年内建成 着重关注外来人口增速 今年减少7项经济增长监测指标 京津冀三地数据将共享)\r\n&lt;/p&gt;'),
(5, '&lt;div&gt;\r\n	&lt;img src=&quot;/upload/2014-01/24/c0ab0cfb82a461a940031691c26c8d5a.jpg&quot; alt=&quot;2014年1月23日上午9时许，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除，4000余位租住在这些“违建群”里的住户搬离。图片来源：法制晚报林晖/CFP&quot; title=&quot;2014年1月23日上午9时许，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除，4000余位租住在这些“违建群”里的住户搬离。图片来源：法制晚报林晖/CFP&quot; width=&quot;550&quot; height=&quot;375&quot; align=&quot;&quot; /&gt;2014年1月23日上午9时许，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除，4000余位租住在这些“违建群”里的住户搬离。图片来源：法制晚报林晖/CFP\r\n&lt;/div&gt;\r\n&lt;div&gt;\r\n	&lt;img src=&quot;/upload/2014-01/24/8b77e484f2e3d27b808f3a7d0f85bd22.jpg&quot; alt=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; title=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; width=&quot;550&quot; height=&quot;366&quot; align=&quot;&quot; /&gt;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP\r\n&lt;/div&gt;\r\n&lt;div&gt;\r\n	&lt;img src=&quot;/upload/2014-01/24/a2bf8983e4174cfd0b2cbe73d7d8fe9c.jpg&quot; alt=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; title=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; width=&quot;550&quot; height=&quot;366&quot; align=&quot;&quot; /&gt;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP\r\n&lt;/div&gt;\r\n&lt;div&gt;\r\n	&lt;img src=&quot;/upload/2014-01/24/0d8a7748d11baf35f8ea730f7d1f1b96.jpg&quot; alt=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; title=&quot;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP&quot; width=&quot;550&quot; height=&quot;366&quot; align=&quot;&quot; /&gt;2014年1月23日，北京朝阳区崔各庄乡奶东村8万平方米违建开始拆除。据了解，这些全部是个人承包后未经规划部门许可私建的房屋，然后出租给个人，形成了多个集经营、仓储、居住用途于一体的“三合一”大院。图片来源：法制晚报林晖/CFP\r\n&lt;/div&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	央广网北京1月24日消息(记者纪乐乐)据中国之声《央广新闻》报道，位于北京市朝阳区的一处总建设面积达8万平米的违章建筑，日前开始整体拆除。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	这个建筑面积达到8万平米的违章建筑位于北京市朝阳区崔各庄乡奶东村。据崔各庄乡副乡长张燕伟介绍，此处违建是个人承包的出租大院，主要用于居住、仓库及经营活动。一般是下面当仓库，上面住人，总共有近4000名流动人口居住。院里私自搭建了大量建筑，并且都没有经过规划部门的批准，属于违法建设。此外，院内违规使用土煤炉、私接乱拉电线等现象也十分普遍，存在着严重的安全隐患。因此，有关部门决定对这一处建筑进行整体拆除。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	这次整体拆除昨天开始，由于这次动用了大量大型机械同时作业，因此在春节前基本可以拆除完毕。这块地，今后将被用作奶东村绿化用地及产业调整用地。对于在此租住的4000名租户，根据张燕伟介绍，在实施拆除前就组织工作人员进行政策宣传讲解，约谈违建当事人，同时对承租户们发放告知信，鼓励配合进行腾退工作。截止到整体拆除前，4000名租户已经全部搬离。目前有关部门并没有对违建当事人提出惩处意见，但是对于租户的补偿金，需要租户与违建当事人根据合同自行解决。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	(原标题：北京朝阳8万平方违建开拆 4000多租户搬离)\r\n&lt;/p&gt;'),
(6, '&lt;div&gt;\r\n	&lt;img src=&quot;/upload/2014-01/24/82b3e7e474418e4eacc5d13c750f2451.jpg&quot; alt=&quot;1月22日，习近平、俞正声、张高丽等在北京人民大会堂同党外人士欢聚一堂，共迎新春。 新华社记者鞠鹏摄&quot; title=&quot;1月22日，习近平、俞正声、张高丽等在北京人民大会堂同党外人士欢聚一堂，共迎新春。 新华社记者鞠鹏摄&quot; width=&quot;550&quot; height=&quot;320&quot; /&gt;&lt;br /&gt;\r\n1月22日，习近平、俞正声、张高丽等在北京人民大会堂同党外人士欢聚一堂，共迎新春。 新华社记者鞠鹏摄\r\n&lt;/div&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	习近平同党外人士共迎新春\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	代表中共中央，向各民主党派、工商联和无党派人士，向统一战线广大成员，致以新春的祝福\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	俞正声、张高丽出席\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	新华网北京1月23日电(记者霍小光) 在春节即将到来之际，中共中央总书记、国家主席、中央军委主席习近平22日下午在人民大会堂同各民主党派中央、全国工商联负责人和无党派人士代表欢聚一堂，共迎新春。他代表中共中央，向各民主党派、工商联和无党派人士，向统一战线广大成员，致以诚挚的问候和新春的祝福。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	中共中央政治局常委、全国政协主席俞正声，中共中央政治局常委、国务院副总理张高丽出席。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	民革中央主席万鄂湘、民盟中央主席张宝文、民建中央主席陈昌智、民进中央主席严隽琪、农工党中央主席陈竺、致公党中央主席万钢、九三学社中央主席韩启德、台盟中央主席林文漪、全国工商联主席王钦敏和无党派人士代表林毅夫、邓中翰等应邀出席。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	陈竺代表各民主党派中央、全国工商联和无党派人士致辞。他指出，2013年，以习近平同志为总书记的中共中央带领全国人民团结奋斗，谋局开篇大略已定，首战奏捷大势已成。中共十八届三中全会对全面深化改革作出了总体部署，作为中国特色社会主义参政党，我们一定要与中国共产党风雨同舟、肝胆相照，做挚友、做诤友，讲真话、建诤言，加强自身建设，旗帜鲜明支持改革，既要献良策，也要出大力，彰显中国共产党领导的多党合作和政治协商制度的旺盛活力。他还就加强党外人士民主监督工作等提出意见和建议。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	在听取了陈竺同志致辞后，习近平发表重要讲话。他首先表示，再过几天，就是农历马年春节了。我念两副报纸上刊登的春联送给大家，一副是“骏马追风扬气魄，寒梅傲雪见精神”，另一副是“昂首扬鬃，骏马舞东风，追求梦想；斗寒傲雪，红梅开大地，实现复兴”。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	习近平指出，2013年，对我们国家来说，是很不平凡的一年。面对复杂多变的国际形势和艰巨繁重的国内改革发展稳定任务，中国共产党紧紧依靠包括各民主党派、工商联和无党派人士在内的全国各族人民，共同战胜了各种困难和挑战，取得了新的显著成就。在过去的一年里，各民主党派、工商联和无党派人士适应时代要求，充分发挥自身优势，主动奋发有为，同中国共产党一道，推动统一战线和多党合作事业向前发展，为全面建成小康社会作出了新的贡献。习近平代表中共中央，向各民主党派、工商联和无党派人士，向统一战线广大成员表示衷心的感谢。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	习近平指出，做好今年各项工作，需要中国共产党同各民主党派、工商联和无党派人士加强团结合作，共同不懈努力。一个篱笆三个桩，一个好汉三个帮。实践证明，建立新中国，建设新中国，开拓改革路，实现中国梦，都需要各党派团体和各界人士齐心努力。越是处于改革攻坚期，越需要汇集众智、增强合力；越是处于发展关键期，越需要凝聚人心、众志成城。希望同志们积极引导所联系群众，凝聚广泛共识，积聚强大能量，深入考察调研，提出真知灼见，让党和政府看问题更全面，作决策更科学。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	习近平强调，协商民主是我国社会主义民主政治的重要组成部分，是我国社会主义民主政治的特有形式和独特优势，也是中国共产党执政和决策的重要方式。希望同志们更加主动发展好协商民主，不断提高协商民主成效和水平。中国共产党各级组织特别是领导干部要以开阔的胸襟、平等的心态、民主的作风广纳群言、广集众智，丰富协商民主形式，增强民主协商实效，为民主党派、工商联和无党派人士发挥作用创造有利条件。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	习近平指出，坚持和发展中国特色社会主义，要求中国共产党加强自身建设，也要求各参政党加强自身建设。希望同志们准确把握建设中国特色社会主义参政党的基本要求，继承优良传统，把握时代要求，不断提高政治把握能力、参政议政能力、组织领导能力、合作共事能力，努力把中国特色社会主义参政党建设提高到一个新的水平。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	最后，习近平以龙马精神、万马奔腾、快马加鞭、马到成功同与会者共勉。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	栗战书，各民主党派中央、全国工商联有关负责人，中央有关部门负责同志参加活动。\r\n&lt;/p&gt;'),
(7, '&lt;p&gt;\r\n	中新网1月24日电 据香港《文汇报》报道，香港机场警署23日早晨发生警员佩枪走火事件。一名机场特警在机场警署更衣室内更换枪袋期间，疑在拔出Glock-17曲尺手枪时突然走火惊动同袍，幸无人受伤，警方相信事件仅属意外，列作“警员意外走火”处理，由机场警区刑事部跟进调查是否涉及人为疏忽，抑或枪械故障所致。\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	事发23日上午11时38分，一名隶属机场特警组的警员，在东涌航膳西路8号机场警署内更衣室准备更换枪袋时，他配备的Glock-17手枪突意外走火一响。同袍听闻枪声纷赶至查看，并一度通知救护车到场戒备，其后证实无人受伤。警署高层接获报告即派员封锁现场调查，军械鉴证科专家稍后到场将涉事手枪检走化验。警方发言人表示，警队就装备的保管及使用有严格的规定，如证实有人员违规定必严肃处理。\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	有熟悉枪械人士表示，不排除有人在为佩枪退弹匣时，将手指放进扳机护弓内导致不慎走火，不过真正原因有待当局调查。\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	警员佩枪走火事件时有发生，去年7月25日，水警小艇队一名警员在昂船洲水警小艇分区基地枪房交回Glock-17手枪，在退膛时亦不慎“走火”一响，幸无人受伤。\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	Glock-17曲尺手枪是奥地利生产，口径9㎜，弹匣可装17发子弹，射程80米。手枪采双扳机结构，即主扳机前加装小扳机作保险杆，先压下小扳机，才能扣动主扳机；扳机力逾5磅，而供执法机构用的枪械，扳机力更达11磅，以避免误扣“走火”。警方于2007年引入Glock-17，飞虎队、机场特警、俗称G4的警队保护要人组、水警小艇队皆使用Glock-17作佩枪。\r\n&lt;/p&gt;'),
(8, '&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	中新网1月24日电 据香港《明报》网站消息，正在福建访问的香港特首梁振英，今早(24日)出席闽港经贸合作交流会。他在会上致辞称，港人思想开放，愿意接受外来人。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	他在致辞时说，香港是开放社会、移民社会，港人思维开放，愿意接受外来人，这是香港文化一部分，有信心邀请福建的朋友来香港打拼，借助香港作为平台“走出去”。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	梁振英称，期望特区政府与福建一起探讨合作新商机，又说希望随团访问的工商专业人，在会上就如何加强福建与香港合作，多提意见，推动两地经贸关系。\r\n&lt;/p&gt;'),
(9, '&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	小伙列车上卖WiFi引发乘客对列车WiFi的向往\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	成都商报记者 张舒\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	实习生 朱茜西\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	核心提示\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	铁路通信信号方面的专家介绍，铁路上火车内的WiFi覆盖仍是取决于运营商的地面信号覆盖，铁路线路难免会经过山区、隧道、人烟稀少的偏远地区，因覆盖率低，环境复杂，信号自然较差。此外，技术方面也存在一定障碍。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	WiFi信号在川内列车上能否使用，和周围人一起分享？昨日，成都商报记者从成都铁路局以及交通运输通信信号方面的专家处得知，四川作为一个山区铁路较为集中的区域，进出川通道成昆、宝成、成渝等线路桥隧比较高，手机信号尚且难以保证，利用手机上网进行WiFi热点覆盖效果不会很好。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	不过，令人期待的是，目前西南交通大学正在联合国内电信设备制造商，与铁路部门联合，设计试验一种专业的“列车WiFi”系统，通过车载信号收发设备，将车外移动和联通等提供的2G、3G甚至4G信号转换为车内WiFi信号，供车内旅客使用，不久将使得国内动车旅客WiFi上网成为现实。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	现状\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	山区太多\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	川内列车用WiFi效果难保证\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	乘坐过川内列车的旅客都有这样的感觉，在进入山区路段后，手机信号时有时无，断断续续，上网、打电话基本成奢望。据成铁局数据统计，进出川重要干线当中，南下，1100公里的成昆线成铁局管内达到700多公里，其中桥隧比达到41%；北上，宝成线桥隧比为10.8%，“虽然比例不高，但很多线路都是河边，都是沿着山脚行走，这部分线路信号难以保证。”成铁局相关负责人介绍。同样北上的一条通道达成线，桥隧比达到28.6%。成渝动车组主要经过遂成、遂渝线，两条线路桥隧比超过43.9%和46.4%，所以成渝动车组信号不够理想。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	该负责人表示，成铁局管内的山区铁路在全国路网中都是最多的，如果需要让手机通讯、3G等信号全覆盖，必须在隧道等覆盖更多的运营商信号，技术较难且工作量较大。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	原因\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	信号覆盖率低\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	速度越快信号越差\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	“目前我们局管内尚未发现有旅客在火车上用WiFi赚钱。”成铁局相关负责人表示，手机信号问题尚没有解决，WiFi的使用效果肯定也不会很好。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	为何WiFi使用效果不好？就此，成都商报记者咨询了铁路通信信号方面的专家、西南交通大学信息科学与技术学院通信工程系主任方旭明教授。目前火车上手机2G、3G、甚至4G信号等都是由各大运营商的地面网络提供，这种列车WiFi如同苹果、安卓手机的个人热点功能，将手机信号如3G信号转化为WiFi路由器信号。“因此，铁路上火车内的WiFi覆盖仍是取决于运营商的地面信号覆盖，铁路线路难免会经过山区、隧道、人烟稀少的偏远地区，因覆盖率低，环境复杂，信号自然较差。”方旭明说。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	除了信号覆盖方面的问题，技术方面也存在一定障碍。方旭明说，移动通信系统是一种类似于蜂窝的系统，是由一个个蜂窝状小区连接而成，当列车高速通过这些小区时，会导致频繁的小区切换，会增加通信中断概率。“理论上说，速度越快的列车，通信质量也就越差。”\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	解决\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	设车载收发设备\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	成渝动车正测试专业“列车WiFi”\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	但事实上，保证列车运营、调度的铁路专用移动通信系统在系统设计时就充分考虑了上述问题，信号质量远高于公用移动通信系统，但两者使用不同的网络，在频段和系统制式上都不同，“前者都是沿着铁路线专门覆盖和优化，所以信号质量得以保障。”方旭明说，未来有望在下一代铁路专用移动通信系统（即LTE-R）中，拿出部分资源兼顾旅客网络通信需求，旅客也可以使用铁路专用的高质量移动通信网络，到那时，在列车上的旅客上网问题就可以得到根本解决。\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	&lt;br /&gt;\r\n&lt;/p&gt;\r\n&lt;p style=&quot;text-indent:2em;&quot;&gt;\r\n	此外，方教授还透露，目前西南交通大学正在联合国内电信设备制造商，与铁路部门一起，设计试验一种专业的“列车WiFi”系统，同样是利用运营商的外部信号，但可以克服普通手机等移动设备功率较小、遭遇列车金属外壳严重衰减等问题，利用专业车载设备，将车外2G、3G甚至4G信号转换为车内WiFi信号，供车内旅客使用，“现阶段该设备正在成渝动车组列车上进行测试运行，有望不久后投用。”方旭明说。\r\n&lt;/p&gt;');

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_position_relation`
--

INSERT INTO `dux_position_relation` (`data_id`, `content_id`, `pos_id`, `start_time`, `stop_time`, `image`, `title`, `content`, `sequence`, `site`) VALUES
(1, 3, 1, 1390738640, 1453853840, '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_replace`
--

INSERT INTO `dux_replace` (`replace_id`, `key`, `content`, `num`) VALUES
(1, '中新网', '网易网', 1);

-- --------------------------------------------------------

--
-- 转存表中的数据 `dux_tags`
--

INSERT INTO `dux_tags` (`tag_id`, `name`, `click`, `quote`, `site`) VALUES
(31, '动车', 7, 1, 1),
(1, '梁振英', 2, 1, 1),
(2, '香港文化', 1, 1, 1),
(3, '外来人', 40, 1, 1),
(4, '明报', 2, 1, 1),
(5, '香港特首', 2, 1, 1),
(6, '机场特警', 2, 1, 1),
(7, '佩枪走火', 2, 1, 1),
(8, '特警', 2, 1, 1),
(9, '走火', 1, 1, 1),
(10, '习近平', 2, 1, 1),
(11, '春联', 1, 1, 1),
(12, '党外人士', 1, 1, 1),
(13, '2014年春节', 1, 1, 1),
(14, '万平', 1, 1, 1),
(15, '违建', 1, 1, 1),
(16, '租户', 1, 1, 1),
(17, '相结合', 1, 1, 1),
(18, '王文杰', 5, 1, 1),
(19, '北京', 1, 1, 1),
(20, '常住', 1, 1, 1),
(21, '监测', 1, 1, 1),
(22, '出租车', 2, 1, 1),
(23, '上海', 3, 1, 1),
(24, 'WiFi', 36, 2, 1),
(25, '森林火灾', 2, 1, 1),
(26, '应急响应', 3, 1, 1),
(27, '紧急', 2, 1, 1),
(28, '快递公司', 3, 1, 1),
(29, '黎某', 4, 1, 1),
(30, '柳州', 3, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `dux_tags_relation`
--

CREATE TABLE IF NOT EXISTS `dux_tags_relation` (
  `content_id` int(10) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL,
  KEY `aid` (`content_id`),
  KEY `tid` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dux_tags_relation`
--

INSERT INTO `dux_tags_relation` (`content_id`, `tag_id`) VALUES
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(7, 6),
(7, 7),
(7, 8),
(7, 9),
(6, 10),
(6, 11),
(6, 12),
(6, 13),
(5, 14),
(5, 15),
(5, 16),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(3, 22),
(3, 23),
(3, 24),
(2, 26),
(2, 27),
(2, 28),
(1, 29),
(1, 30),
(1, 31),
(9, 25),
(9, 32);

-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
